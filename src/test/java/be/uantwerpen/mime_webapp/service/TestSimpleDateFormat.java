package be.uantwerpen.mime_webapp.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class TestSimpleDateFormat {

	@Test
	public void testSimpleDateFormat() throws ParseException{
		String input = "22AUG2014:20:54:07";
		String format="ddMMMyyyy:HH:mm:ss";
		String oformat = "yyyy-MM-dd HH:mm";
		Date d = new SimpleDateFormat(format).parse(input);
		System.out.println(new SimpleDateFormat(oformat).format(d));
	}
}
