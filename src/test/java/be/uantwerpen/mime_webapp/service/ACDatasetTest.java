package be.uantwerpen.mime_webapp.service;

import java.io.File;

import be.uantwerpen.ldataminining.preprocessing.ApplyStructureUtils;
import be.uantwerpen.ldataminining.preprocessing.SQLQueryOnArff;

public class ACDatasetTest {

	//@Test
	public void test() throws Exception{
		File input = new File("/data/upload/UA_MEAS_EVENTS_9156.arff");
		if(input.exists())
			ApplyStructureUtils.dropZeroInformationAttributes(input, new File("temp/test.arff"), 3000);
	}
	
	//@Test
	public void test2() throws Exception{
		String query = "SELECT " +
		"EXTRACT (YEAR FROM UTCTIME) AS YEAR, " +
		"EXTRACT (MONTH FROM UTCTIME) AS MONTH, " +
		"EXTRACT (DAY FROM UTCTIME) AS DAY, " +
		"avg(MP509), avg(MP501), avg(MP602), avg(MP2700), avg(MP1001), avg(MP604), avg(MP1008), avg(MP1214) " +
		"FROM file " +
		"GROUP BY YEAR,MONTH,DAY";
		new SQLQueryOnArff().runQuery(new File("/data/upload/UA_MEAS_EVENTS_579-filtered-zero-columns-date-transformed-query.arff"), 
				query);

	}
}
