package be.uantwerpen.mime_webapp.dao;

import org.junit.Test;

import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.Project;

public class ProjectRepositoryTest{

	@Test
	public void testFindByName() {
		Project project = new Project();
		project.setName("unittest");
		FileItem item = new FileItem();
		item.setLogicalName("iris");
		item.setFilename("iris.arff");
		project.add(item);
		ProjectRepository repo = new ProjectRepository();
		repo.saveProject(project);
		repo.findAll();
		repo.removeProject("unittest");
	}
}