package be.uantwerpen.datamining;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.datamining.pattern_mining.MakePatternOccurrencesSpan;

public class TestMakePatternOccurrencesSpan {

	@Test
	public void testFindItemsetOccurrenceWithMinimalSPan() {
		MakePatternOccurrencesSpan service = new MakePatternOccurrencesSpan();
		List<List<String>> sequence = new ArrayList<>();
		List<String> cols = Arrays.asList("value","label");
		List<String> row = Arrays.asList("8;7;8;8;10;10;10;10;8;7;10;11;11;11;11;10;9;8;7;6;6;6;6;4;5;4;4;3;3",
				"0;0;0;0; 0; 0; 0; 0;0;0; 0; 0; 0; 0; 0; 0;0;0;0;0;0;0;0;0;0;-1;0;0;0");
		for(String column: cols) {
			int colIdx = cols.indexOf(column);
			String[] valuesColumn = row.get(colIdx).split("\\s*;\\s*");
			List<String> ssequence = new ArrayList<>();
			for(String val: valuesColumn) {
				ssequence.add(String.format("%s=%s", column, val));
			}
			sequence.add(ssequence);
		}
		//test 1:
		String[] pattern = "value=8 value=7 label=-1".split(" "); 
		List<Integer> occurrenceWithMinSpan = service.findItemsetOccurrenceWithMinimalSPan(sequence, pattern);
		System.out.println(occurrenceWithMinSpan);
		for(int d=0; d<sequence.size(); d++) {
			System.out.println(sequence.get(d).subList(Collections.min(occurrenceWithMinSpan), Collections.max(occurrenceWithMinSpan)+1));
		}
		//test 2:
		String[] pattern2 = "value=8 value=7 value=10".split(" "); 
		occurrenceWithMinSpan = service.findItemsetOccurrenceWithMinimalSPan(sequence, pattern2);
		System.out.println(occurrenceWithMinSpan);
		for(int d=0; d<sequence.size(); d++) {
			System.out.println(sequence.get(d).subList(Collections.min(occurrenceWithMinSpan), Collections.max(occurrenceWithMinSpan)+1));
		}
		//test 3:
		String[] pattern3 = "value=8 value=7 value=2".split(" "); 
		occurrenceWithMinSpan = service.findItemsetOccurrenceWithMinimalSPan(sequence, pattern3);
		assertTrue(occurrenceWithMinSpan == null);
	}

	@Test
	public void testFindSPOccurrenceWithMinimalSPan() {
		MakePatternOccurrencesSpan service = new MakePatternOccurrencesSpan();
		List<List<String>> sequence = new ArrayList<>();
		List<String> cols = Arrays.asList("value","label");
		List<String> row = Arrays.asList("8;7;8;8;10;10;10;10;8;7;10;11;11;11;11;10;9;8;7;6;6;6;8;7;5;4;4;3;3",
				"0;0;0;0; 0; 0; 0; 0;0;0; 0; 0; 0; 0; 0; 0;0;0;0;0;0;0;0;0;0;-1;0;0;0");
		for(String column: cols) {
			int colIdx = cols.indexOf(column);
			String[] valuesColumn = row.get(colIdx).split("\\s*;\\s*");
			List<String> ssequence = new ArrayList<>();
			for(String val: valuesColumn) {
				ssequence.add(String.format("%s=%s", column, val));
			}
			sequence.add(ssequence);
		}
		//test 1:
		String[] pattern = "value=8 value=7 label=-1".split(" "); 
		List<Integer> occurrenceWithMinSpan = service.findSPOccurrenceWithMinimalSPan(sequence, pattern);
		System.out.println(occurrenceWithMinSpan);
		for(int d=0; d<sequence.size(); d++) {
			System.out.println(sequence.get(d).subList(Collections.min(occurrenceWithMinSpan), Collections.max(occurrenceWithMinSpan)+1));
		}
		//test 2:
		String[] pattern2 = "value=7 value=10 value=11".split(" "); 
		occurrenceWithMinSpan = service.findSPOccurrenceWithMinimalSPan(sequence, pattern2);
		System.out.println(occurrenceWithMinSpan);
		for(int d=0; d<sequence.size(); d++) {
			System.out.println(sequence.get(d).subList(Collections.min(occurrenceWithMinSpan), Collections.max(occurrenceWithMinSpan)+1));
		}
	}
}
