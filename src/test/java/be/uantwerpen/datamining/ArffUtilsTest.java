package be.uantwerpen.datamining;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;

import org.junit.Test;

import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;

public class ArffUtilsTest {

	@Test
	public void testGetClassColumnIndex() throws Exception{
		File test = new File("src/test/resources/iris_discretized.arff");
		int idx = ArffUtils.getClassColumnIndex(test);
		assertTrue(idx == 5);
	}
	
	@Test
	public void testTransformAnyDateColumns() throws Exception{
		File file = makeDummyData();
		ArffDenseUtils.transformAnyDateColumns(
				file, 
				new File("temp/test-dates2.arff"), 
				"yyyy-MMM-dd", "yyyy-MM-dd");
		System.out.println(IOUtils.readFileFlat(new File("temp/test-dates2.arff")));
	}
	
	@Test
	public void testUpdateNames() throws Exception{
		File file = makeDummyData();
		ArffUtils.updateAttributeNames(file, Arrays.asList(new String[]{"a1","a2","a3","b0"}));
		System.out.println(IOUtils.readFileFlat(file));
	}

	@Test
	public void testSparseFile() throws Exception{
		File file = makeDummyData();
		ArffUtils.updateAttributeTypes(file, Arrays.asList("NUMERIC","NUMERIC","NUMERIC","NUMERIC"));
		System.out.println(IOUtils.readFileFlat(file));
		File file2 = makeDummyDataSparse();
		ArffUtils.updateAttributeTypes(file2, Arrays.asList("NUMERIC","NUMERIC","NUMERIC","NUMERIC"));
		System.out.println(IOUtils.readFileFlat(file2));
	}
	
	private File makeDummyData() throws Exception {
		String data = "@relation test\n"
				+ "@attribute d1 STRING\n"
				+ "@attribute d2 STRING\n"
				+ "@attribute d3 INT\n"
				+ "@attribute d4 STRING\n"
				+ "@data\n"
				+ "2014-JAN-21,2012-01-01,12,2012-MAY-15\n"
				+ "2013-FEB-01,2014-03-20,4,?\n";
		File f = new File("temp/test-dates.arff");
		IOUtils.saveFile(data, f);
		return f;
	}
	
	private File makeDummyDataSparse() throws Exception {
		String data = "@relation test\n"
				+ "@attribute d1 STRING\n"
				+ "@attribute d2 STRING\n"
				+ "@attribute d3 INT\n"
				+ "@attribute d4 STRING\n"
				+ "@data\n"
				+ "{0 2014-JAN-21,1 2012-01-01,2 12}\n"
				+ "{0 2013-FEB-01,1 2014-03-20}\n";
		File f = new File("temp/test-dates-sparse.arff");
		IOUtils.saveFile(data, f);
		return f;
	}
	
}
