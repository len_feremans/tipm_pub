package be.uantwerpen.datamining;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.datamining.pattern_mining.MakeWindows;
import be.uantwerpen.ldataminining.utils.IOUtils;

public class MakeWindowsTest {

	@Test
	public void testMakeWindowsNoOverlap() throws IOException{
		File input = new File("./data/upload/ambient_temp.arff");
		File output = new File("./data/upload/ambient_temp_test.arff");
		if(!input.exists())
			throw new RuntimeException("Unit test file not found:" + input.getAbsolutePath());
		MakeWindows.makeWindows(input, output, 10, 10);
		IOUtils.printHead(input,20);
		IOUtils.printTail(input, 20);
		IOUtils.printHead(output,20);
		IOUtils.printTail(output, 20);
	}
	
	@Test
	public void testMakeWindowsOverlap() throws IOException{
		File input = new File("./data/upload/ambient_temp.arff");
		File output = new File("./data/upload/ambient_temp_test.arff");
		if(!input.exists())
			throw new RuntimeException("Unit test file not found:" + input.getAbsolutePath());
		MakeWindows.makeWindows(input, output, 10, 5);
		IOUtils.printHead(output,20);
		IOUtils.printTail(output, 20);
	}
	
	@Test
	public void testMakeWindowsTime() throws IOException{
		File input = new File("./data/upload/ambient_temp.arff");
		File output = new File("./data/upload/ambient_temp_test.arff");
		if(!input.exists())
			throw new RuntimeException("Unit test file not found:" + input.getAbsolutePath());
		MakeWindows.makeWindowsTime(input, output, 12*60*60, 6*60*60);
		IOUtils.printHead(output,20);
		IOUtils.printTail(output, 20);
	}
}
