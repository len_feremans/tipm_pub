package be.uantwerpen.datamining;

import java.io.File;

import org.junit.Test;

import be.uantwerpen.ldataminining.preprocessing.SQLQueryOnArff;
import be.uantwerpen.ldataminining.utils.IOUtils;

public class SQLQueryOnArffTest {

	@Test 
	public void test() throws Exception{
		String data = "@relation students\n"
				+ "@attribute id INTEGER\n"
				+ "@attribute score NUMERIC\n"
				+ "@attribute examDate DATE\n"
				+ "@data\n"
				+ "1, 18.2, 2014-01-20T12:00:00\n"
				+ "2, 15.0, 2014-01-20T13:00:00\n";
		File datafile = new File("temp/student-test.arff");
		IOUtils.saveFile(data, datafile);
		File output1 = new SQLQueryOnArff().runQuery(datafile, 
				"SELECT EXTRACT (YEAR FROM examDate) AS YEAR, " + 
				"EXTRACT (MONTH FROM examDate) AS MONTH, " + 
				"EXTRACT (DAY FROM examDate) AS DAY, " + 
				"avg(score) " + 
				"FROM file GROUP BY YEAR, MONTH, DAY");
		System.out.println(IOUtils.readFileFlat(output1));
	}
	
	@Test 
	public void test2() throws Exception{
		String data = "@relation students\n"
				+ "@attribute id INTEGER\n"
				+ "@attribute score NUMERIC\n"
				+ "@attribute examDate DATE\n"
				+ "@data\n"
				+ "1, 18.2, 2014-01-20T12:00:00\n"
				+ "2, 15.0, 2014-01-20T13:00:00\n";
		File datafile = new File("temp/student-test.arff");
		IOUtils.saveFile(data, datafile);
		File output1 = new SQLQueryOnArff().runQuery(datafile, 
				"SELECT * " + 
				"FROM file GROUP BY ID");
		System.out.println(IOUtils.readFileFlat(output1));
	}
}
