package be.uantwerpen.datamining;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import be.uantwerpen.ldataminining.model.Dataset;
import be.uantwerpen.ldataminining.model.Dataset.ClassAttributeOrder;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.AutoConvertCSVToArff;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;

public class AutoCSVToArffTest {
	
	@Test
	public void testConvertToArf2f() throws Exception{
		File testFile = new File("./data/upload/train_data.csv");
		ClassAttributeOrder order = ClassAttributeOrder.LAST;
		Dataset set = new Dataset();
		set.setFile(testFile);
		set.setSeperator(',');
		set.setClassAttributeOrder(order);
		set.setPreProcessedFile(new File("./data/upload/train_data.arff"));
		new AutoConvertCSVToArff().convertCSVToArff(set, true);
		new AutoConvertCSVToArff().run(testFile, true);
		assertEquals(IOUtils.countLines(testFile) - 1,ArffUtils.getNumberOfRows(set.getPreProcessedFile()));
	}
	
}
