package be.uantwerpen.datamining;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.datamining.pattern_mining.MineUsingSPMF;
import be.uantwerpen.ldataminining.utils.IOUtils;

public class MineUsingSPMFTest {

	@Test
	public void testRunItemsetMining() throws Exception {
		File input = new File("./data/upload/ambient_temp-007.arff");
		MineUsingSPMF service = new MineUsingSPMF();
		String[] algorithms = new String[] {"Eclat", "Charm_bitset","Charm_MFI", "AprioriRare"}; 
		List<String> cols = Arrays.asList("value");
		for(String algo: algorithms) {
			File outputPatterns = service.runItemsetMining(input, cols, algo, 5);
			IOUtils.printHead(outputPatterns, 5);
			IOUtils.printTail(outputPatterns, 5);
		}
	}
	
	@Test
	public void testRunSequentialPatternMining() throws Exception {
		File input = new File("./data/upload/A10-028.arff");
		MineUsingSPMF service = new MineUsingSPMF();
		String[] algorithms = new String[] {"PrefixSpan", "CM-ClaSP","MaxSP"};
		List<String> cols = Arrays.asList("unique_EVENT_E10");
		for(String algo: algorithms) {
			File outputPatterns = service.runSequentialPatternMining(input, cols, algo, 5);
			IOUtils.printHead(outputPatterns, 5);
			IOUtils.printTail(outputPatterns, 5);
		}
	}
}
