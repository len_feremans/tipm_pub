package be.uantwerpen.datamining;


import java.io.File;

import org.junit.Test;

import be.uantwerpen.ldataminining.preprocessing.ArffStatistics;

public class ArffStatisticsTest {
	
	@Test
	public void printStats() throws Exception{
		File test = new File("src/test/resources/iris_discretized.arff");
		System.out.println(ArffStatistics.getAllAttributeStatisticsFast(test));
	}
	
	@Test
	public void printStats2() throws Exception{
		File test = new File("./data/upload/A10-009.arff");
		System.out.println(ArffStatistics.getAllAttributeStatisticsFast(test));
	}
}
