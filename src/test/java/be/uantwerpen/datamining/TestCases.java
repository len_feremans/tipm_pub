package be.uantwerpen.datamining;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.junit.Test;

import be.uantwerpen.ldataminining.preprocessing.CSVUtils;

public class TestCases {

	@Test
	public void testParse() throws ParseException {
		String value = "-6,23929";
		NumberFormat nf_in = NumberFormat.getNumberInstance(Locale.GERMANY);
		double val = nf_in.parse(value).doubleValue();
		//System.out.println(Double.valueOf(value));
		System.out.println(value);
		System.out.println(CSVUtils.escapeValueForArff(value));
	}
}
