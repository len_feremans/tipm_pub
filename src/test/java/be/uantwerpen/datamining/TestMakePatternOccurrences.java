package be.uantwerpen.datamining;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import be.uantwerpen.datamining.pattern_mining.MakePatternOccurrences;
import be.uantwerpen.datamining.pattern_mining.MineUsingSPMF;
import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.mime_webapp.model.PatternSet;

public class TestMakePatternOccurrences {

	@Test
	public void testGroupByWindow() throws IOException {
		File input = new File("./data/upload/taxi-009.arff");
		Table t = MakePatternOccurrences.groupByWindow(input, 0, 1000);
		for(int i=0; i<50 & i < t.getRows().size(); i++) {
			System.out.println(t.getRows().get(i));
		}
	}
	
	@Test
	public void testPatternOccurrencesItemsets() throws IOException {
		File arff = new File("./data/upload/ambient_temp-010.arff");
		PatternSet set = new PatternSet();
		set.setColumns("value,label");
		set.setFilename("ambient_temp-010.arff_Eclat_[value, label]_10.0_patterns.csv");
		set.setType("itemsets");
		ListMap<Integer,Integer> occurrences = new MakePatternOccurrences().makeExactPatternOccurrences(arff, set);
	}
	
	@Test
	public void testPatternOccurrencesItemsetRunSPMF() throws Exception {
		File arff = new File("./data/upload/ambient_temp-010.arff");
		MineUsingSPMF miner = new MineUsingSPMF();
		File pattternFile = miner.runItemsetMining(arff, Arrays.asList("value","label"), "Apriori" , 10);
		IOUtils.printHead(pattternFile);
		PatternSet set = new PatternSet();
		set.setColumns("value,label");
		set.setFilename(pattternFile.getName());
		set.setType("itemsets");
	}
	
	@Test
	public void testPatternOccurrencesSP1Dimensional() throws IOException {
		File arff = new File("./data/upload/ambient_temp-010.arff");
		PatternSet set = new PatternSet();
		set.setColumns("value");
		set.setFilename("ambient_temp-010.arff_MaxSP_[value]_20.0_patterns.csv");
		set.setType("sequential pattern");
		ListMap<Integer,Integer> occurrences = new MakePatternOccurrences().makeExactPatternOccurrences(arff, set);
	}
}