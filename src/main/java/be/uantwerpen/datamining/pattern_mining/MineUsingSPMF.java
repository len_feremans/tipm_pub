package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.util.List;

import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.CommandLineUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.mime_webapp.Settings;
import ca.pfv.spmf.algorithmmanager.AlgorithmManager;
import ca.pfv.spmf.algorithmmanager.DescriptionOfAlgorithm;

public class MineUsingSPMF {

	public File runItemsetMining(File arff, List<String> columns, String algorithm, double support) throws Exception {
		//check if algorithm exists
		if(!checkAlgorithmExists(algorithm, true))
			return null;
		Pair<File,File> dictFile_transFile_pair = ArffToSPMF.arffToSPMFTransactionalDatabase(arff, columns);
		//java -jar spmf.jar run Apriori contextPasquier99.txt output.txt 40% 
		File log = new File("./temp/pattern_mining_" + arff.getName() + ".log");
		File outputRaw = new File("./temp/" + algorithm + "_out_raw_" + arff.getName() + ".txt");
		long timeOutSeconds = 10;
		CommandLineUtils.runCommandInUserDir(new String[]{"java","-jar", new File(Settings.SPMF_JAR).getAbsolutePath(), "run", algorithm, 
				dictFile_transFile_pair.getSecond().getAbsolutePath(), 
				outputRaw.getAbsolutePath(), "" + support + "%"}, log,
				timeOutSeconds);
		IOUtils.printHead(log, 100);
		String logStr = IOUtils.readFileFlat(log);
		if(logStr.contains("Exception")) {
			throw new Exception("Exception during mining with SPMF.\n" + logStr);
		}
		File outputReadable = new File(Settings.FILE_FOLDER, arff.getName() + "_" + algorithm + "_" + CollectionUtils.join(columns,"_") + "_" + support + "_patterns.csv");
		ArffToSPMF.saveOutputReadable(arff.getName(), dictFile_transFile_pair.getFirst(), outputRaw, outputReadable);
		if(outputReadable.exists() && !IOUtils.isEmptyFile(outputReadable)) {
			dictFile_transFile_pair.getFirst().delete();
			dictFile_transFile_pair.getSecond().delete();
			log.delete();
			outputRaw.delete();
		}
		return outputReadable;
	}

	public File runSequentialPatternMining(File arff, List<String> columns, String algorithm, double support) throws Exception {
		//check if algorithm exists
		if(!checkAlgorithmExists(algorithm, false))
			return null;
		Pair<File,File> dictFile_transFile_pair = ArffToSPMF.arffToSPMFSequenceDatabase(arff, columns);
		//java -jar spmf.jar run Apriori contextPasquier99.txt output.txt 40% 
		File log = new File("./temp/pattern_mining_" + arff.getName() + ".log");
		File outputRaw = new File("./temp/" + algorithm + "_out_raw_" + arff.getName() + ".txt");
		long timeOutSeconds = 10;
		CommandLineUtils.runCommandInUserDir(new String[]{"java","-jar", new File(Settings.SPMF_JAR).getAbsolutePath(), "run", algorithm, 
				dictFile_transFile_pair.getSecond().getAbsolutePath(), 
				outputRaw.getAbsolutePath(), "" + support + "%"}, log,
				timeOutSeconds);
		IOUtils.printHead(log, 100);
		String logStr = IOUtils.readFileFlat(log);
		if(logStr.contains("Exception")) {
			throw new Exception("Exception during mining with SPMF.\n" + logStr);
		}
		File outputReadable = new File(Settings.FILE_FOLDER, arff.getName() + "_" + algorithm + "_" + CollectionUtils.join(columns,"_") + "_" + support + "_patterns.csv");
		ArffToSPMF.saveOutputReadable(arff.getName(), dictFile_transFile_pair.getFirst(), outputRaw, outputReadable);
		if(outputReadable.exists() && !IOUtils.isEmptyFile(outputReadable)) {
			dictFile_transFile_pair.getFirst().delete();
			dictFile_transFile_pair.getSecond().delete();
			log.delete();
			outputRaw.delete();
		}
		return outputReadable;
	}
	
	private boolean checkAlgorithmExists(String algorithm, boolean itemsets) {
		try {
			DescriptionOfAlgorithm description = AlgorithmManager.getInstance().getDescriptionOfAlgorithm(algorithm);
			if(description == null) {
				throw new RuntimeException(String.format("Algorithm %s not found in SPMF", algorithm));
			}
			else {
				System.out.println("Found:" + description.getName() + " - " + description.getAlgorithmCategory() + " (" + description.getImplementationAuthorNames() + ")");
				for(int i = 0; i< description.getParametersDescription().length; i++) {
					System.out.println("   Parameter:" + description.getParametersDescription()[i].name + "  " + description.getParametersDescription()[i].example);
				}
				if(itemsets && !description.getAlgorithmCategory().equals("FREQUENT ITEMSET MINING")) {
					throw new RuntimeException(String.format("Algorithm %s is not for FREQUENT ITEMSET mining, but %s", algorithm, description.getAlgorithmCategory()));
				}
				else if(!itemsets && !description.getAlgorithmCategory().equals("SEQUENTIAL PATTERN MINING")){
					throw new RuntimeException(String.format("Algorithm %s is not for SEQUENTIAL PATTERN mining, but %s", algorithm, description.getAlgorithmCategory()));
				}
				
				return true; 
			}
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
