package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.ldataminining.utils.Triple;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.model.PatternSet;

/**
 * Copy pasted from MakePatternOccurrences.
 * Do no only maintain witch pattern occur in witch windows, but also the index of values with the minimal window.
 * @author lfereman
 *
 */
public class MakePatternOccurrencesSpan {

	//return triples like <0,1,<t1,t2,t2>>, where 0 is first window, 1 is first pattern, 
	//and t1,t2,t3 are timestamps of occurrences of items of pattern 1 in window 0.
	//Note that timestamps are relative positions to window start
	public  List<Triple<Integer, Integer, List<Integer>>> makeExactPatternOccurrencesWithSpan(File arff, PatternSet patternSet) throws IOException {
		Table table = MakePatternOccurrences.groupByWindow(arff, -1, -1);
		Table patterns = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + patternSet.getFilename()));
		List<String> columns = table.getRows().get(0);
		int windowIdx = columns.indexOf("Window");
		if(windowIdx == -1) {
			throw new RuntimeException("Window not found");
		}
		List<Triple<Integer, Integer, List<Integer>>> exactPatternOccurrences = null;
		if(patternSet.getType().equals("itemsets")){
			exactPatternOccurrences = makeOccurrencesItemset(patternSet, table, patterns, columns);
		}
		else if(patternSet.getType().equals("sequential patterns")) {
			exactPatternOccurrences = makeOccurrencesSP(patternSet, table, patterns, columns);
		}
		else {
			throw new RuntimeException("Pattern type unknown:" + patternSet.getType());
		}
		return exactPatternOccurrences;
	}

	private List<Triple<Integer, Integer, List<Integer>>> makeOccurrencesItemset(
			PatternSet patternSet,  Table table, Table patterns, List<String> columns) {
		int windowIdx = columns.indexOf("Window");
		Timer timer = new Timer("makeExactPatternOccurrencesSpan");
		List<Triple<Integer, Integer, List<Integer>>>  exactPatternOccurrences =new ArrayList<>();
		for(int i=1; i<table.getTotalSize();i++) {
			if(i%100 ==0)
				timer.progress(i,table.getTotalSize());
			List<String> row = table.getRows().get(i);
			int window  = Integer.valueOf(row.get(windowIdx));
			List<List<String>> sequence = getWindowValues(patternSet, columns, row);
			for(int j=1; j<patterns.getTotalSize();j++) {
				String[] pattern = patterns.getRows().get(j).get(0).split(" "); //e.g. like value=8 value=7 label=-1 
				List<Integer> bestMatch = findItemsetOccurrenceWithMinimalSPan(sequence, pattern);
				if(bestMatch != null) {
					exactPatternOccurrences.add(new Triple<>(window,j,bestMatch));
				}
			}
		}
		timer.end();
		return exactPatternOccurrences;
	}

	private List<List<String>> getWindowValues(PatternSet patternSet, List<String> columns, List<String> row) {
		List<List<String>> sequence = new ArrayList<>();
		for(String column: patternSet.getColumns().split(",")) {
			int colIdx = columns.indexOf(column);
			String[] valuesColumn = row.get(colIdx).split(";");
			List<String> ssequence = new ArrayList<>();
			for(String val: valuesColumn) {
				ssequence.add(String.format("%s=%s", column, val));
			}
			sequence.add(ssequence);
		}
		return sequence;
	}

	//check which pattern match in witch window, and also the minimal window (i.e. occurrences with minimal span)
	//Algorithm similar to simple version of computeMinimalWindow in FCI
	//e.g.  "value","label"
	//		"8;7;8;8;10;10;10;10;8;7;10;11;11;11;11;10;9;8;7;6;6;6;6;4;5;4;4;3;3"
	//		"0;0;0;0; 0; 0; 0; 0;0;0; 0; 0; 0; 0; 0; 0;0;0;0;0;0;0;0;0;0;-1;0;0;0"
	//													 - - - - - - - - -
	//       -> value=8, value=7, label=-1 has a minimal window of 8
	public List<Integer> findItemsetOccurrenceWithMinimalSPan(List<List<String>> windowValues, String[] pattern) {
		List<Integer> lastMatch = new ArrayList<>();
		List<Integer> bestMatch = null;
		int bestSpan = Integer.MAX_VALUE;
		for(String item: pattern) {
			lastMatch.add(-1);
		}
		for(int t=0; t<windowValues.get(0).size(); t++) { //loop over time
			for(int d=0; d<windowValues.size(); d++) { //loop over dimensions
				String val = windowValues.get(d).get(t);
				for(int itemIdx=0; itemIdx<pattern.length; itemIdx++) { //loop over pattern items
					if(val.equals(pattern[itemIdx])) {
						//update currentBestMatch
						lastMatch.set(itemIdx,t);
						//check if "complete", that is an occurrence
						if(!lastMatch.contains(-1)) {
							//check if span is minimal
							int span = Collections.max(lastMatch) - Collections.min(lastMatch);
							if(span < bestSpan) {
								bestMatch = new ArrayList<>(lastMatch);
								bestSpan = span;
							}
						}
					}	
				}
			}
		}
		return bestMatch;
	}
	
	private List<Triple<Integer, Integer, List<Integer>>>  makeOccurrencesSP(PatternSet patternSet, Table table, Table patterns, List<String> columns) {
		int windowIdx = columns.indexOf("Window");
		Timer timer = new Timer("makeExactPatternOccurrencesSpan");
		List<Triple<Integer, Integer, List<Integer>>> exactPatternOccurrences = new ArrayList<>();
		for(int i=1; i<table.getTotalSize();i++) { //for each window
			if(i%100 ==0)
				timer.progress(i,table.getTotalSize());
			List<String> row = table.getRows().get(i);
			int window  = Integer.valueOf(row.get(windowIdx));
			List<List<String>> sequence = getWindowValues(patternSet, columns, row);
			//check which pattern match
			for(int j=1; j<patterns.getTotalSize();j++) {
				String[] pattern = patterns.getRows().get(j).get(0).split(" "); //e.g. like value=7 value=8 value=8 label=1
				List<Integer> bestMatch = findSPOccurrenceWithMinimalSPan(sequence, pattern);
				if(bestMatch != null) {
					exactPatternOccurrences.add(new Triple<>(window,j,bestMatch));
				}
			}
		} //end for-each window
		return exactPatternOccurrences;
	}

	//check which pattern match in witch window, and also the minimal window (i.e. occurrences with minimal span)
	//e.g.  "value","label"
	//		"8;7;8;8;10;10;10;10;8;7;10;11;11;11;11;10;9;8;7;6;6;6;8;7;5;4;4;3;3"
	//		"0;0;0;0; 0; 0; 0; 0;0;0; 0; 0; 0; 0; 0; 0;0;0;0;0;0;0;0;0;0;-1;0;0;0"
	//													 		   - - - -
	//       -> value=8, value=7, label=-1 has a minimal window of 4
	public List<Integer> findSPOccurrenceWithMinimalSPan(List<List<String>> windowValues, String[] pattern) {
		List<Triple<Integer,Integer,Integer>> search = new ArrayList<>(); //e.g. <firstOccurces, current, last>
		search.add(new Triple<>(0, 0, windowValues.get(0).size()));
		//find all occurrences
		for(int itemIdx=0; itemIdx<pattern.length; itemIdx++) { //loop over pattern items
			String patternValue = pattern[itemIdx];
			List<Triple<Integer,Integer,Integer>> searchNext = new ArrayList<>();
			for(Triple<Integer,Integer,Integer> window: search) { //loop over segments in windows
				List<Integer> occurrences = getOccurrences(windowValues, patternValue, window.getSecond(), window.getThirth());
				if(itemIdx == 0) {
					for(int occurenceT: occurrences) {
						searchNext.add(new Triple<>(occurenceT, occurenceT+1, window.getThirth()));
					}
				}
				else {
					if(occurrences.size() > 0) {
						searchNext.add(new Triple<>(window.getFirst(), occurrences.get(0)+1, window.getThirth()));
					}
				}
			}
			search = searchNext;
		}
		//return window with smallest span
		Triple<Integer,Integer,Integer> bestWindow = null;
		int bestSpan = Integer.MAX_VALUE;
		for(Triple<Integer,Integer,Integer> window: search) {
			int span = window.getSecond() - window.getFirst();
			if(span < bestSpan) {
				bestSpan = span;
				bestWindow = window;
			}
		}
		if(bestWindow == null)
			return null;
		else {
			//return actual occurrences of each item, not only span
			int start = bestWindow.getFirst();
			int end = bestWindow.getSecond();
			List<Integer> occurrencesAllItems = new ArrayList<>();
			for(int itemIdx=0; itemIdx<pattern.length; itemIdx++){
				String patternValue = pattern[itemIdx];
				List<Integer> occurrencesCurrent = getOccurrences(windowValues, patternValue,start,end);
				int t = occurrencesCurrent.get(0);
				occurrencesAllItems.add(t);
				start = t+1;
			}
			return occurrencesAllItems;
		}
	}
	
	private List<Integer> getOccurrences(List<List<String>> windowValues, String value, int start, int end){
		List<Integer> occurrences = new ArrayList<Integer>();
		for(int t=start; t<end; t++) { //loop over time
			for(int d=0; d<windowValues.size(); d++) { //loop over dimensions
				String val = windowValues.get(d).get(t);
				if(val.equals(value)) {
					occurrences.add(t);
				}
			}
		}
		return occurrences;
	}
}
