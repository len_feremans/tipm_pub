package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.model.PatternSet;

public class MakePatternOccurrences {

	public static boolean DEBUG = false;
	
	public ListMap<Integer,Integer> makeExactPatternOccurrences(File arff, PatternSet patternSet) throws IOException {
		Table table = groupByWindow(arff, -1, -1);
		Table patterns = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + patternSet.getFilename()));
		//e.g. patterns like:
		// ["pattern","support"],
		// ["value=10","67"],
		// ["value=10 value=9","54"]
		List<String> columns = table.getRows().get(0);
		int windowIdx = columns.indexOf("Window");
		if(windowIdx == -1) {
			throw new RuntimeException("Window not found");
		}
		ListMap<Integer, Integer> exactPatternOccurrences = null;
		if(patternSet.getType().equals("itemsets")){
			exactPatternOccurrences = makeOccurrencesItemset(patternSet, table, patterns, columns);
			//post-condition: check that support matches with stored support
			if(DEBUG) {
				postConditioncheckSupport(1, table, patterns, exactPatternOccurrences);
				postConditioncheckSupport(patterns.size()-1, table, patterns, exactPatternOccurrences);
			}
		}//end itemsets
		else if(patternSet.getType().equals("sequential patterns")) {
			exactPatternOccurrences = makeOccurrencesSP(patternSet, table, patterns, columns);
			if(DEBUG) {
				postConditioncheckSupport(1, table, patterns, exactPatternOccurrences);
				postConditioncheckSupport(patterns.size()-1, table, patterns, exactPatternOccurrences);
			}
		}
		else {
			throw new RuntimeException("Pattern type unknown:" + patternSet.getType());
		}
		
		return exactPatternOccurrences;
	}

	private ListMap<Integer, Integer>  makeOccurrencesItemset(PatternSet patternSet, Table table, Table patterns, List<String> columns) {
		int windowIdx = columns.indexOf("Window");
		Timer timer = new Timer("makeExactPatternOccurrences");
		ListMap<Integer,Integer> exactPatternOccurrences = new ListMap<>(true);
		for(int i=1; i<table.getTotalSize();i++) {
			if(i%100 ==0)
				timer.progress(i,table.getTotalSize());
			List<String> row = table.getRows().get(i);
			int window  = Integer.valueOf(row.get(windowIdx));
			//e.g. like
			// 	[	"timestamp","value","label","Window"]
			//	   ["2013-07-04 03:00:00 - 2013-07-07 15:00:00",
			//		"8;7;8;8;10;10;10;10;8;7;10;11;11;11;11;10;9;8;7;6;6;6;6;4;5;4;4;3;3",
			//		"0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;-1;0;0;0",
			//		"0"],
			Set<String> transactionItemset = new HashSet<>();
			//make set of values, for each dimension
			for(String column: patternSet.getColumns().split(",")) {
				int colIdx = columns.indexOf(column);
				String[] valuesColumn = row.get(colIdx).split(";");
				for(String val: valuesColumn) {
					transactionItemset.add(String.format("%s=%s", column, val));
				}
			}
			//check which pattern match
			for(int j=1; j<patterns.getTotalSize();j++) {
				String[] pattern = patterns.getRows().get(j).get(0).split(" "); //e.g. like value=10 value=9, or 
				boolean matchAll = true;
				for(String item: pattern) {
					if(!transactionItemset.contains(item)) {
						matchAll = false;
						break;
					}
				}
				if(matchAll) {
					exactPatternOccurrences.put(window, j); //patterns are indexed by rowNumber, first pattern is 1
				}
			}
		}
		timer.end();
		return exactPatternOccurrences;
	}
	
	private ListMap<Integer,Integer> makeOccurrencesSP(PatternSet patternSet, Table table, Table patterns, List<String> columns) {
		int windowIdx = columns.indexOf("Window");
		Timer timer = new Timer("makeExactPatternOccurrences");
		ListMap<Integer,Integer> exactPatternOccurrences = new ListMap<>(true);
		for(int i=1; i<table.getTotalSize();i++) { //for each window
			if(i%100 ==0)
				timer.progress(i,table.getTotalSize());
			List<String> row = table.getRows().get(i);
			int window  = Integer.valueOf(row.get(windowIdx));
			//e.g. like
			// 	[	"timestamp","value","label","Window"]
			//	   ["2013-07-04 03:00:00 - 2013-07-07 15:00:00",
			//		"8;7;8;8;10;10;10;10;8;7;10;11;11;11;11;10;9;8;7;6;6;6;6;4;5;4;4;3;3",
			//		"0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;-1;0;0;0",
			//		"0"],
			List<List<String>> sequence = new ArrayList<>();
			//make list of sets of values, for each dimension
			for(String column: patternSet.getColumns().split(",")) {
				int colIdx = columns.indexOf(column);
				String[] valuesColumn = row.get(colIdx).split(";");
				List<String> ssequence = new ArrayList<>();
				for(String val: valuesColumn) {
					ssequence.add(String.format("%s=%s", column, val));
				}
				sequence.add(ssequence);
			}
			//check which pattern match
			for(int j=1; j<patterns.getTotalSize();j++) {
				String[] pattern = patterns.getRows().get(j).get(0).split(" "); //e.g. like value=7 value=8 value=8 label=1
				boolean match = matchSequentialPattern(sequence, pattern);
				if(match) {
					exactPatternOccurrences.put(window, j); //patterns are indexed by rowNumber, first pattern is 1
				}
			}
		} //end for-each window
		return exactPatternOccurrences;
	}

	private static boolean matchSequentialPattern(List<List<String>> sequence, String[] pattern) {
		int start = 0;
		int end = sequence.get(0).size(); //NOTE: assuming all dimensions have windows of same size
		for(String item: pattern) {
			boolean itemMatch = false;
			outer:
			for(int k=start; k<end; k++) {
				for(int d=0; d<sequence.size(); d++) {
					String sequence_val = sequence.get(d).get(k);
					if(sequence_val.equals(item)) {
						itemMatch = true;
						start = k+1;
						break outer;
					}
				}
			}
			if(!itemMatch) {
				return false;
			}
		}
		return true;
	}

	private static void postConditioncheckSupport(int patternId, Table table, Table patterns, ListMap<Integer, Integer> exactPatternOccurrences) {
		int supportFirstPatterns = Integer.valueOf(patterns.getRows().get(patternId).get(1));
		int supportComputed = 0;
		for(Integer window: exactPatternOccurrences.keySet()) {
			if(exactPatternOccurrences.get(window).contains(patternId)) {
				supportComputed+=1;
			}
		}
		System.out.println("Pattern " + patterns.getRows().get(patternId));
		System.out.println("Total occurrences: " + supportComputed);
		System.out.println("First 3 occurrences:");
		for(int win=0; win<3; win++) {
			List<Integer> patternsInWindow = exactPatternOccurrences.get(win);
			boolean contains = patternsInWindow != null && patternsInWindow.contains(patternId);
			System.out.format("%s\t\t\t%s\n",table.getRows().get(win+1), contains);//just for testing
		}
		if(supportFirstPatterns != supportComputed)
			System.err.println("makeExactPatternOccurrences: support does not match.");
	}
	
	public static ListMap<Integer,List<String>> groupByWindowGetListMap(File arff, int from, int to) throws IOException {
		Table table = new ArffDenseUtils().loadArff(arff, from, to);
		List<String> columns = table.getRows().get(0);
		List<String> types = ArffUtils.getAttributeValues(arff);
		int window_index = columns.indexOf("Window");
		if(window_index == -1)
			return null;
		//parse windows
		List<String> windows = table.getColumnValues(window_index);
		windows.remove(0);
		TreeSet<Integer> window_values = new TreeSet<Integer>();
		for(String window: windows) {
			String[] windowValues = window.split(";"); //in case of overlapping windows, can be multiple, e.g. "0;1"
			for(String windowValue: windowValues) {
				if(windowValue.equals("?")) { //should not happen
					System.err.println("Window value is ? (last window is " + window_values.last() + ")");
					continue;
				}
				window_values.add(Integer.valueOf(windowValue));
			}

		}
		//group by window
		ListMap<Integer,List<String>> groupByWindow = new ListMap<>(true);
		for(List<String> row: table.getRows().subList(1, table.getRows().size())) {
			String[] overlappingWindows = row.get(window_index).split(";"); //in case of overlapping windows, can be multiple, e.g. "0;1"
			for(String windowStr: overlappingWindows) {
				if(windowStr.equals("?"))
					continue;
				Integer window = Integer.valueOf(windowStr);
				groupByWindow.put(window,row);
			}
		}
		System.out.println("Group by. #" + groupByWindow.values().size());
		return groupByWindow;
	}
			
	//e.g. Make output like
	//	   ["timestamp","value","label","Window"]
	//	   ["2013-07-04 03:00:00 - 2013-07-07 15:00:00",
	//		"8;7;8;8;10;10;10;10;8;7;10;11;11;11;11;10;9;8;7;6;6;6;6;4;5;4;4;3;3",
	//		"0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;-1;0;0;0",
	//		"0"],
	public static Table groupByWindow(File arff, int from, int to) throws IOException {
		List<String> columns = ArffUtils.getAttributeNames(arff);
		List<String> types = ArffUtils.getAttributeValues(arff);
		ListMap<Integer,List<String>> groupByWindow = groupByWindowGetListMap(arff, from, to);
		Set<Integer> window_values = groupByWindow.keySet();
		//create table/grouped representation:
		//1. for timestamps: return first/last values within group
		//2. for reals/integers/discrete values: return list of values
		//3. for window just return window
		Table tableNew = new Table();
		tableNew.addRow(columns);
		for(Integer window: window_values) {
			List<List<String>> row_group = groupByWindow.get(window);
			List<String> new_row = new ArrayList<String>();
			for(int i=0; i<types.size(); i++) {
				String name = columns.get(i);
				String type = types.get(i);
				if(type.startsWith("DATE") || i ==0) { //if date, or first column, take first and last column
					String new_value = row_group.get(0).get(i) + " - " + row_group.get(row_group.size()-1).get(i);
					new_row.add(new_value);
				}
				else if(name.equals("Window")) {
					String new_value = String.valueOf(window);
					new_row.add(new_value);
				}
				else { //make list
					List<String> values = new ArrayList<>();
					for(List<String> row: row_group) {
						String value = row.get(i);
						values.add(value);
					}
					String new_value = CollectionUtils.join(values,";");
					new_row.add(new_value);
				}
			}
			tableNew.addRow(new_row);
		}
		return tableNew;
	}
}
