package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.ldataminining.model.CountMap;
import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.NaturalOrderComparator;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
public class ArffToSPMF {
	
	public static Map<String,Integer> oneHotEncodingItems(List<String> names, List<List<String>> rows, List<String> columns_filtered, File dictOutput) throws IOException {
		CountMap<String> items = new CountMap<>();
		for(List<String> row: rows) {
			for(String name: columns_filtered) {
				int colIdx = names.indexOf(name);
				if(CSVUtils.isEmptyValueInCSV(row.get(colIdx))) { //if empty skip
					continue;
				}
				items.add(String.format("%s=%s", names.get(colIdx), row.get(colIdx)));
			}
		}
		System.out.format("Found %d items: %s\n", items.getMap().size(), items.getKeysSortedDescending());
		List<String> itemsSorted = new ArrayList<>(items.getMap().keySet());
		Collections.sort(itemsSorted);
		IOUtils.saveFile(itemsSorted,dictOutput);
		Map<String,Integer> itemsSortedMap = new HashMap<>();
		for(int i=0; i< itemsSorted.size(); i++) {
			itemsSortedMap.put(itemsSorted.get(i),i+1); //Items are 1-indexed!
		}
		return itemsSortedMap;
	}
	
	//Note: //copy-pasted from TableLoadController.groupByWindow
	public static Pair<TreeSet<Integer>, ListMap<Integer,List<String>>> groupByWindow(List<String> names, List<List<String>> rows) {
		int window_index = names.indexOf("Window");
		if(window_index == -1)
			throw new RuntimeException("Window not found!");	
		TreeSet<Integer> windowIdsSorted = new TreeSet<Integer>();
		for(List<String> row: rows) {
			String windowStr = row.get(window_index);
			String[] windowValues = windowStr.split(";"); //in case of overlapping windows, can be multiple, e.g. "0;1"
			for(String windowValue: windowValues) {
				windowIdsSorted.add(Integer.valueOf(windowValue));
			}
		}
		ListMap<Integer,List<String>> groupByWindow = new ListMap<>();
		for(List<String> row: rows) {
			String[] overlappingWindows = row.get(window_index).split(";");
			for(String windowStr: overlappingWindows) {
				Integer window = Integer.valueOf(windowStr);
				groupByWindow.put(window,row);
			}
		}
		return new Pair<>(windowIdsSorted, groupByWindow);
	}
	
	static Pair<File,File> arffToSPMFTransactionalDatabase(File arff, List<String> columns_filtered) throws IOException {
		List<String> names = ArffUtils.getAttributeNames(arff);
		List<String> types = ArffUtils.getAttributeValues(arff);
		List<List<String>> rows = ArffUtils.loadDataMatrix(arff);
		
		System.out.format("toTransactionalDatabase: columns: %s\n".format(CollectionUtils.join(columns_filtered)));
		//1. make item representation
		File dict = new File("./temp/", IOUtils.getFileWithDifferentExtension(IOUtils.getFileWithDifferentSuffix(arff, "-dict"),"txt").getName());
		Map<String,Integer> itemsSortedMap = oneHotEncodingItems(names,rows,columns_filtered,dict);
		//2. Group by window
		Pair<TreeSet<Integer>, ListMap<Integer,List<String>>> pair = groupByWindow(names, rows);
		TreeSet<Integer> windowIdsSorted = pair.getFirst();
		ListMap<Integer,List<String>> groupByWindow = pair.getSecond();
		//3. create table/grouped representation:
		System.out.println("Creating transaction database. Nr of windows: " + windowIdsSorted.size());
		List<String> transactions = new ArrayList<String>();
		for(Integer window: windowIdsSorted) {
			List<List<String>> row_group = groupByWindow.get(window);
			Set<Integer> transaction = new TreeSet<>();
			for(String column: columns_filtered) {
				int colIdx = names.indexOf(column);
				for(List<String> row: row_group) {
					String value = row.get(colIdx);
					if(!CSVUtils.isEmptyValueInCSV(value)) {
						Integer itemId = itemsSortedMap.get(String.format("%s=%s", column, value));
						transaction.add(itemId);
					}
				}
			}
			transactions.add(CollectionUtils.join(transaction, " "));
		}
		File transactiondb = new File("./temp", 
				IOUtils.getFileWithDifferentExtension(IOUtils.getFileWithDifferentSuffix(arff, "-transactions"),"txt").getName());
		IOUtils.saveFile(transactions,transactiondb);
		//DEBUG:
		postConditionCheckTranslation(dict, transactions, transactiondb, names, itemsSortedMap, groupByWindow);
		return new Pair<>(dict,transactiondb);
	}

	private static void postConditionCheckTranslation(File dict, List<String> transactions, File transactiondb, List<String> names, Map<String,Integer> itemsSortedMap, ListMap<Integer,List<String>> groupByWindow) throws IOException {
		System.out.println("postConditionCheckTranslation:");
		Map<String, String> itemToLabel = loadTranslation(dict);
		for(int i=0; i<1; i++) {
			System.out.println("Window " + i + ":");
			List<List<String>> groupByRow = groupByWindow.get(i);
			if(groupByRow == null)
				return;
			List<List<String>> series = new ArrayList<>();
			System.out.println("Raw data:");
			for(int col=0; col<names.size(); col++) {
				List<String> valsCol = new ArrayList<>();
				for(List<String> row: groupByRow) {
					valsCol.add(row.get(col));
				}
				series.add(valsCol);
				System.out.println(valsCol);
			}
			System.out.println("Raw data translated:");
			int k=0;
			for(List<String> serie: series) {
				List<Integer> valsTranslated = new ArrayList<>();
				for(String val: serie) {
					Integer itemId = itemsSortedMap.get(String.format("%s=%s", names.get(k), val));
					valsTranslated.add(itemId);
				}
				System.out.println(valsTranslated);
				k++;
			}
			System.out.println("Transaction:");
			System.out.println( transactions.get(i));
			String[] transaction = transactions.get(i).split(" ");
			String translated = "";
			for(String transactionItem: transaction) {
				translated += itemToLabel.get(transactionItem) + " ";
			}
			System.out.println("Translated back:");
			System.out.println(translated);
		}
	}
	
	static Pair<File,File> arffToSPMFSequenceDatabase(File arff, List<String> columns_filtered) throws IOException {
		List<String> names = ArffUtils.getAttributeNames(arff);
		List<String> types = ArffUtils.getAttributeValues(arff);
		List<List<String>> rows = ArffUtils.loadDataMatrix(arff);
		
		System.out.format("toSequenceDatabase: columns: %s\n".format(CollectionUtils.join(columns_filtered)));
		//1. make item representation
		File dict = new File("./temp/", IOUtils.getFileWithDifferentExtension(IOUtils.getFileWithDifferentSuffix(arff, "-dict"),"txt").getName());
		Map<String,Integer> itemsSortedMap = oneHotEncodingItems(names,rows,columns_filtered,dict);
		//2. Group by window
		Pair<TreeSet<Integer>, ListMap<Integer,List<String>>> pair = groupByWindow(names, rows);
		TreeSet<Integer> windowIdsSorted = pair.getFirst();
		ListMap<Integer,List<String>> groupByWindow = pair.getSecond();
		//3. create table/grouped representation:
		System.out.println("Creating sequence database. Nr of windows: " + windowIdsSorted.size());
		List<String> transactions = new ArrayList<String>();
		for(Integer window: windowIdsSorted) {
			List<List<String>> row_group = groupByWindow.get(window);
			List<Set<Integer>> transaction = new ArrayList<>(); //note: if multivariate/mixed-type timeseries: add items of different dimensions
			for(List<String> row: row_group) {
				Set<Integer> itemsetAtTimestampK = new TreeSet<Integer>();
				for(String column: columns_filtered) {
					int colIdx = names.indexOf(column);
					String value = row.get(colIdx);
					if(!CSVUtils.isEmptyValueInCSV(value)) {
						Integer itemId = itemsSortedMap.get(String.format("%s=%s", column, value));
						itemsetAtTimestampK.add(itemId);
					}
				}
				transaction.add(itemsetAtTimestampK);
			}
			// SPMF format:
			// 1 -1 1 2 3 -1 1 3 -1 4 -1 3 6 -1 -2
			// The first line represents a sequence where the itemset {1} is followed by the itemset {1, 2, 3}, 
			// followed by the itemset {1, 3}, followed by the itemset {4}, followed by the itemset {3, 6}
			String s = "";
			for(Set<Integer> set: transaction) {
				if(set.isEmpty())
					continue;
				s += CollectionUtils.join(set, " ") + " -1 ";
			}
			s += "-2";
			transactions.add(s);
		}
		File transactiondb = new File("./temp", 
				IOUtils.getFileWithDifferentExtension(IOUtils.getFileWithDifferentSuffix(arff, "-sequencedb"),"txt").getName());
		IOUtils.saveFile(transactions,transactiondb);
		return new Pair<>(dict,transactiondb);
	}
	
	@SuppressWarnings("unchecked")
	public static void saveOutputReadable(String datasetName, File dictFile, File outputSPMF, File outputCSVReadable)
			throws IOException {
		//For itemsets output like: 
		//0 #SUP: 36
		//25 76 87 117 119  #SUP: 134
		//For sequential patterns output like:
		//1 -1 15 -1  #SUP: 6
		Map<String, String> itemToLabel = loadTranslation(dictFile);
		//parse
		List<Pair<String,Integer>> readablePatterns = new ArrayList<>();
		List<String> lines = IOUtils.readFile(outputSPMF);
		for(String line: lines) {
			String[] tokens = line.split("\\s+");
			List<String> patternReadable = new ArrayList<>();
			for(int i=0; i < tokens.length -2; i++) {
				String item = tokens[i];
				if(item.equals("-1"))
					continue;
				String label = itemToLabel.get(item);
				patternReadable.add(label);
			}
			int support = Integer.valueOf(tokens[tokens.length-1]);
			if(!line.contains("-1")) { //sort itemsets
				Collections.sort(patternReadable, new NaturalOrderComparator());
			}
			readablePatterns.add(new Pair<String,Integer>(CollectionUtils.join(patternReadable, " "), support));
		}
		//sort
		readablePatterns.sort(new PatternComparator());
		System.out.println("Found " + readablePatterns.size() + " patterns.");
		//save
		List<String> linesOutput = new ArrayList<String>();
		linesOutput.add("pattern,support");
		for(Pair<String,Integer> pattern: readablePatterns) {
			linesOutput.add(String.format("%s,%s", pattern.getFirst(), pattern.getSecond()));
		}
		IOUtils.saveFile(linesOutput, outputCSVReadable);
	}
	
	public static class PatternComparator implements Comparator<Pair<String,Integer>>{

		NaturalOrderComparator comp = new NaturalOrderComparator();
		
		@Override
		public int compare(Pair<String, Integer> a, Pair<String, Integer> b) {
			if(b.getSecond().compareTo(a.getSecond()) == 0) { //if support is equal, compare on pattern itself
				return comp.compare(a.getFirst(), b.getFirst());
			}
			else {
				return b.getSecond().compareTo(a.getSecond());
			}
		}
	}
	
	private static Map<String, String> loadTranslation(File dictFile) throws IOException {
		List<String> labels = IOUtils.readFile(dictFile);
		Map<String,String> itemToLabel = new HashMap<>();
		for(int i=0; i< labels.size(); i++) {
			itemToLabel.put(String.valueOf(i + 1), labels.get(i));
		}
		return itemToLabel;
	}
}
