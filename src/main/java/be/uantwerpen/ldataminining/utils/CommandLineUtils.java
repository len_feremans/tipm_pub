package be.uantwerpen.ldataminining.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Utilities code for running programs command line.
 * 
 * @author lfereman
 *
 */
public class CommandLineUtils {

	/**
	 * Used by runPython
	 * 
	 * @author lfereman
	 *
	 */
	/*
	public static class ReturnRunCode
	{
		private boolean killed;
		private String errorLog;
		private String consoleOutput;
		private boolean errors;

		public boolean isKilled() {
			return killed;
		}
		public void setKilled(boolean killed) {
			this.killed = killed;
		}
		public String getErrorLog() {
			return errorLog;
		}
		public void setErrorLog(String errorLog) {
			this.errorLog = errorLog;
		}
		public String getConsoleOutput() {
			return consoleOutput;
		}
		public void setConsoleOutput(String consoleOutput) {
			this.consoleOutput = consoleOutput;
		}
		public boolean isErrors() {
			return errors;
		}
		public void setErrors(boolean errors) {
			this.errors = errors;
		}

	}
	*/
	
	//see http://stackoverflow.com/questions/14165517/
	public static class StreamGobbler extends Thread {
		InputStream is;
		PrintStream os;
	
		public StreamGobbler(InputStream is, PrintStream os) {
			this.is = is;
			this.os = os;
		}
	
		@Override
		public void run() {
			try {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line = null;
				while ((line = br.readLine()) != null)
				{
					os.println(line);
					os.flush();
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	/*
	public static int runCommand(String[] command) throws Exception{
		return CommandLineUtils.runCommand(command, System.out, System.err, null);
	}

	public static int runCommand(String[] command, File log) throws Exception{
		log.getParentFile().mkdirs();
		PrintStream out = new PrintStream(new FileOutputStream(log));
		int result = CommandLineUtils.runCommand(command,out, out, null);
		out.close();
		return result;
	}
	*/
	
	public static int runCommandInUserDir(String[] command, File log, long timeOutSeconds) throws FileNotFoundException, InterruptedException{
		log.getParentFile().mkdirs();
		PrintStream out = new PrintStream(new FileOutputStream(log));
		int result = CommandLineUtils.runCommand(command,out, out, new File(System.getProperty("user.home")), timeOutSeconds);
		out.flush();
		out.close();
		return result;
	}

	/**
	 * Runs program command line.
	 * 
	 * e.g. runCommand({"ls", "-lah"}, System.out, System.err , '/temp') 
	 * 
	 * Command: specify program and all arguments as array
	 * PrintStream out and err: can be System.out or IOUtils.getPrintStream(logFile) for file
	 * Workdir: null or directory where command is executed
	 * @throws InterruptedException 
	 * 
	 */
	public static int runCommand(final String[] command, PrintStream out, PrintStream err, File workingDirectory, long timeOutSeconds) 
			throws InterruptedException{
		try{
			long timeStart = System.currentTimeMillis();
			String started = String.format(">Running %s\n", CollectionUtils.join(command, " "));
			System.out.print(started); 
			if(out != System.out)
				out.print(started);
			//see http://stackoverflow.com/questions/14165517/
			ProcessBuilder pb = new ProcessBuilder().command(command);
			if(workingDirectory != null)
				pb.directory(workingDirectory);
			pb.redirectErrorStream(true);
			final Process p = pb.start();
			CommandLineUtils.StreamGobbler errorGobbler = new CommandLineUtils.StreamGobbler(p.getErrorStream(),  err);
			// any output?
			CommandLineUtils.StreamGobbler outputGobbler = new CommandLineUtils.StreamGobbler(p.getInputStream(),  out);
			// start gobblers
			outputGobbler.start();
			errorGobbler.start();
			// add shutdown hook -> If Java is killed, subprocessed should be killed
			// unsure if this works...
			// See also http://stackoverflow.com/questions/269494/how-can-i-cause-a-child-process-to-exit-when-the-parent-does
			Thread closeChildThread = new Thread() {
			    public void run() {
			    	try{
			    		p.exitValue(); //Check if process active, if not exception
			    	}
			    	catch(IllegalThreadStateException e)
			    	{
			    		//process is active -> Destoy
			    		System.out.format("Killing %s\n", command[0]);
			    		p.destroy();
			    	}
			    }
			};
			Runtime.getRuntime().addShutdownHook(closeChildThread); 
			if(!p.waitFor(timeOutSeconds, TimeUnit.SECONDS)) {
				p.destroy();
				err.flush();
				out.flush();
				String msg = String.format("Triggered timeout of %d seconds after running %s\n", timeOutSeconds, CollectionUtils.join(command, " "));
				throw new InterruptedException(msg);
			}
			else {
				double elapsed = (System.currentTimeMillis() - timeStart)/1000.0;
				if(p.exitValue() != 0)
				{
					String msg = String.format("Error running %s", Arrays.toString(command));
					err.println( msg); 
					System.err.println( msg);
				}
				String ended = String.format("<Finished took %5.2f seconds", elapsed);
				System.out.println(ended); out.println(ended);
				err.flush();
				err.close();
				out.flush(); //new?
				out.close();
				return p.exitValue();
			}
		}
		catch(java.lang.InterruptedException interupted) {
			String msg = String.format("Triggered timeout of %d seconds after running %s\n", timeOutSeconds, CollectionUtils.join(command, " "));
			System.err.println(msg);
			throw new InterruptedException(msg);
		}
		catch(Exception e)
		{
			System.err.format("Error running %s\n", Arrays.toString(command));
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Split string, but ignore whitespace inside quotes
	 * 
	 * Given input like rm "test string" and this string
	 * -> output is [rm,"test string",and,this,string]
	 * @param s
	 * @return
	 */
	/*
	public static String[] smartSplitWhitespace(String s)
	{
		char[] chars = s.toCharArray();
		boolean inQuote = false;
		for(int i=0; i< chars.length; i++)
		{
			if(chars[i] == '"' || chars[i] == '\'' ){
				inQuote =!inQuote;
			}
			if(Character.isWhitespace(chars[i]) && inQuote){
				chars[i] = '@';
			}
		}
		String newString = new String(chars); //replace all whitespace with @
		String[] splitted = newString.split("\\s+");
		for(int i=0; i< splitted.length; i++)
		{
			splitted[i] = splitted[i].replace('@', ' ');
		}
		return splitted;
	}
	*/

	/**
	 * For now just needed by auto correcting homeworks, not datamining, but might come handy
     *
	 */
	/*
	public static ReturnRunCode runSinglePythonScript(File pythonFile, File logFile, File errLogFile, String[] inputs, int waitInSeconds) throws Exception
	{
		assert(pythonFile.exists());
		logFile.getParentFile().mkdirs();
		PrintStream out = new PrintStream(new FileOutputStream(logFile));
		PrintStream err = new PrintStream(new FileOutputStream(errLogFile));
		ReturnRunCode result = new ReturnRunCode();
		String[] command = new String[]{"/Library/Frameworks/Python.framework/Versions/3.4/bin/python3", pythonFile.getAbsolutePath()};
		assert(new File(command[0]).exists());
		ProcessBuilder pb = new ProcessBuilder().command(command);
		pb.redirectErrorStream(true);
		Process p = pb.start();
		StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(),  err);
		StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(),  out);
		outputGobbler.start();
		errorGobbler.start();
		if(inputs != null)
		{
			OutputStream stream = p.getOutputStream();
			for(String input: inputs)
			{
				stream.write(input.getBytes());
				stream.flush();
			}
			stream.close();
		}
		long waited = 0;
		while(waited < waitInSeconds * 1000) //check every 10 ms if process is finished, break after k seconds or process finished
		{
			try{
				p.exitValue();
				break;
			}catch(IllegalThreadStateException ex)
			{
			}
			Thread.sleep(10);
			waited += 10;
		}
		if(waited > waitInSeconds * 1000)
		{
			result.setKilled(true);
			p.destroy();
			System.err.println("Killed");
		}
		else{
			int res = p.exitValue();
			result.setKilled(false);
			result.setErrors(res != 0);
		}
		out.flush();
		err.flush();
		out.close();
		err.close();
		//remove errorLogFile is empty
		result.setErrorLog(IOUtils.readFileFlat(errLogFile));
		errLogFile.delete();
		result.setConsoleOutput(IOUtils.readFileFlat(logFile));
		logFile.delete();
		return result;
	}
	*/
}
