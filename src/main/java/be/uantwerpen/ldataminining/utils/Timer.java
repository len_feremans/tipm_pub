package be.uantwerpen.ldataminining.utils;

public class Timer {

	public static boolean VERBOSE = true;
	private long start;
	private long intermediateStart;
	private String process;
	
	public Timer(String process){
		this.start = System.currentTimeMillis();
		this.intermediateStart = this.start;
		this.lastDisplay = this.start;
		this.process = process;
		if(process.length() > 40){
			this.process = process.substring(0, 40) + "...";
		}
		if(VERBOSE)
			System.out.format(">Started %s\n", process);
	}
	
	public void progress(int i){
		progress(null, i, -1);
	}
	
	public void progress(String message, int i){
		progress(message, i, -1);
	}
	
	public void progress(long i, long total){
		progress(null, i, total);
	}
	
	
	private long lastDisplay = 0;
	
	public void progressPrintEvery10Seconds(long i, long total){
		long end = System.currentTimeMillis();
		long elapsedSinceLastDisplay = (end - lastDisplay);
		long elapsedTotal = (end - start);
		if(VERBOSE && elapsedSinceLastDisplay/1000 > 10){
			//rule of three:
			//   if processing k items takes s milis
			//   then processing 1 item takes s/k milis
			//   and estimated time is (total - k) X s/k milis
			//true but somethimes average time within loop increase.... so take average
			double estimateOneItem = elapsedTotal/(double)i;
			long estimatedMilis =  Math.round((total - i) * estimateOneItem); 
			String estimate = " Expected " +  Utils.milisToStringReadable(estimatedMilis);
			String message = null;
			System.out.format(" Process %s %s: %.2f %% items. Elapsed %s. Total %s.%s\n", 
				process, message == null? "": message, i/(double)total * 100,
				Utils.milisToStringReadable(elapsedSinceLastDisplay),
				Utils.milisToStringReadable(elapsedTotal),
				estimate);
			lastDisplay = System.currentTimeMillis();
		}
	}
	
	
	public void progress(String message, long i, long total){
		long end = System.currentTimeMillis();
		long elapsed = (end - intermediateStart);
		long elapsedTotal = (end - start);
		if(VERBOSE){
			String estimate = "";
			if(total<i)
				total = i;
			if(total > 10){
				//rule of three:
				//   if processing k items takes s milis
				//   then processing 1 item takes s/k milis
				//   and estimated time is (total - k) X s/k milis
				//true but somethimes average time within loop increase.... so take average
				double estimateOneItem = elapsedTotal/(double)i;
				long estimatedMilis =  Math.round((total - i) * estimateOneItem); 
				estimate = " Expected " +  Utils.milisToStringReadable(estimatedMilis);
			}
			System.out.format(" Process %s %s: %-3d/%-3d items. Elapsed %s. Total %s.%s\n", 
				process, message == null? "": message, i,total,
				Utils.milisToStringReadable(elapsed),
				Utils.milisToStringReadable(elapsedTotal),
				estimate);
		}
		this.intermediateStart = System.currentTimeMillis(); 
	}
	
	
	public long end(){
		long end = System.currentTimeMillis();
		long elapsed = (end - start);
		if(VERBOSE)
			System.out.format("<Finished %s. Took %s\n", process, 
					Utils.milisToStringReadable(elapsed));
		return elapsed;
				
	}
	
	public long elapsed(){
		long end = System.currentTimeMillis();
		long elapsed = (end - start);
		return elapsed;
	}
	
	



}
