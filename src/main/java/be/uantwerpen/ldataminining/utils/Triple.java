package be.uantwerpen.ldataminining.utils;

public class Triple<T1,T2,T3> {
	private T1 first;
	private T2 second;
	private T3 thirth;
	
	public Triple(){}
	
	public Triple(T1 first, T2 second, T3 thirth) {
		super();
		this.first = first;
		this.second = second;
		this.thirth = thirth;
	}
	public T1 getFirst() {
		return first;
	}
	public void setFirst(T1 first) {
		this.first = first;
	}
	public T2 getSecond() {
		return second;
	}
	public void setSecond(T2 second) {
		this.second = second;
	}
	public T3 getThirth() {
		return thirth;
	}
	public void setThirth(T3 thirth) {
		this.thirth = thirth;
	}
	
	public String toString() {
		return String.format("<%s,%s,%s>", first, second, thirth);
	}
	
}
