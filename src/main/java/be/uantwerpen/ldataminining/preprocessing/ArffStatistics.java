package be.uantwerpen.ldataminining.preprocessing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import be.uantwerpen.ldataminining.model.CountMap;
import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.MultiCountMap;
import be.uantwerpen.ldataminining.model.SetMap;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils.StreamerArffGeneric;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils.StreamerArffRegular;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.MathUtils;
import be.uantwerpen.ldataminining.utils.Utils;

public class ArffStatistics {

	public static CountMap<String> getClassDistributions(File arffFile) throws Exception{
		final CountMap<String> distributionsClasses = new CountMap<String>();
		final String classIdx = "" + ArffUtils.getAttributeNames(arffFile).indexOf("class");

		if(ArffUtils.isSparse(arffFile)){
			ArffUtils.streamSparseFile(arffFile,  new ArffUtils.StreamerArff() {
				
				@Override
				public void doSomething(String[][] row) {
					for(String[] pair: row){
						if(pair[0].equals(classIdx)){
							distributionsClasses.add(pair[1]);
						}
					}
				}
			});
		}
		else{
			ArffUtils.streamFile(arffFile, new StreamerArffRegular() {
				
				@Override
				public void doSomething(List<String> row) {
					String clzz = row.get(Integer.valueOf(classIdx));
					distributionsClasses.add(clzz);
				}
			});
		}
		return distributionsClasses;
	}

	public static List<Map<String,String>> getAllAttributeStatisticsFast(File input) throws IOException
	{
		final List<String> allAttributes = ArffUtils.getAttributeNames(input);
		final List<String> types = ArffUtils.getAttributeValues(input);
		final Map<String,String> typesIndexed = new HashMap<String,String>();
		for(int i=0; i< allAttributes.size(); i++){
			typesIndexed.put(allAttributes.get(i), types.get(i));
		}
		final CountMap<String> notNullCounts = new CountMap<String>();
		final SetMap<String,String> distinctCounts = new SetMap<String,String>();
		final ListMap<String, Double> numbers = new ListMap<String,Double>();
		final MultiCountMap<String, String> categoryCounts = new MultiCountMap<String, String>();
		final MultiCountMap<String, String> countsFirstValue = new MultiCountMap<String, String>();
		final int totalRows[] = new int[]{0}; 
		
		ArffUtils.streamSpareOrNormalFile(input, new StreamerArffGeneric() {
			
			@Override
			public void doSomething(List<String> attributes, List<String> row) {
				totalRows[0]++;
				for(int i=0; i<row.size() && i < attributes.size();i++){
					String value = row.get(i);
					if(!value.equals("?")){
						//if(i >= attributes.size()) {
						//	System.err.println("Warning: Attributes " + 
						//				CollectionUtils.join(attributes) + " Row: " + CollectionUtils.join(row));
						//	continue;
						//}
						String name = attributes.get(i);
						String type = typesIndexed.get(name);
						notNullCounts.add(name);
						distinctCounts.put(name, value);
						if(type.equals("INTEGER") || type.equalsIgnoreCase("NUMERIC") || type.equals("REAL")){
							numbers.put(name,Utils.asDouble(value));
						}
						if(type.startsWith("{")){
							categoryCounts.add(name,value);
						}
						if(notNullCounts.get(name) == 1){
							countsFirstValue.add(name, value);
						}
						if(countsFirstValue.getMap(name) != null && countsFirstValue.getMap(name).get(value) != null){
							countsFirstValue.add(name,value);
						}
					}
				}
			}
		});
		List<Map<String,String>> statisticsList = new ArrayList<Map<String,String>>();
		for(int i=0; i<allAttributes.size(); i++){
			Map<String,String> statistics = new HashMap<String,String>();
			String name = allAttributes.get(i);
			String type =  types.get(i);
			statistics.put("attribute", name);
			statistics.put("type",type);
			statistics.put("noValues", "" + totalRows[0]); //a bit stupid;
			statistics.put("noNotNull", "" + notNullCounts.get(name));
			statistics.put("noDistinct", "" + 0);
			if(notNullCounts.get(name) > 0){
				statistics.put("noDistinct", "" + distinctCounts.get(name).size());
				if(type.equals("INTEGER") || type.equalsIgnoreCase("NUMERIC") || type.equals("REAL")){
					List<Double> data = numbers.get(name);
					if(data == null)
						continue;
					List<Double> dataNotNull = new ArrayList<Double>();
					for(Double x: data) {
						if(x != null) {
							dataNotNull.add(x);
						}
					}
					List<Double> sorted = new ArrayList<>(dataNotNull);
					Collections.sort(sorted);
					double min = MathUtils.min(dataNotNull);
					double max = MathUtils.max(dataNotNull);
					statistics.put("mean", "" + String.format("%.10f", MathUtils.mean(dataNotNull)));
					statistics.put("std", "" + String.format("%.10f", MathUtils.std(dataNotNull)));
					statistics.put("min", "" + min);
					statistics.put("max", "" + max);
					statistics.put("histogram", CollectionUtils.join(MathUtils.calcHistogram(dataNotNull, min, max,10)));
					statistics.put("histogram-labels", CollectionUtils.join(MathUtils.calcHistogramLegend(dataNotNull, min, max,10)));
					statistics.put("histogram-dens", CollectionUtils.join(MathUtils.calcHistogramEqualDensity(dataNotNull, min, max,10)));
					
					statistics.put("histogram-dens-labels", CollectionUtils.join(MathUtils.calcHistogramLegendEqualDensity(sorted, min, max,10)));
					int[] sigmaCounts = MathUtils.backOfEnvelopTest(dataNotNull);
					statistics.put("t-distribution", CollectionUtils.join(sigmaCounts));
					statistics.put("t-distribution-expected", CollectionUtils.join(MathUtils.backOfEnvelopTestExpectationND(dataNotNull)));
					statistics.put("IQ_01", String.format("%.6f", sorted.get(sorted.size()/100)));
					int quantiles = 10;
					int amount = sorted.size()/10;
					List<Double> quantilesListLeft = new ArrayList<>();
					for(int q=0;q<=quantiles;q++) {
						quantilesListLeft.add(sorted.get(MathUtils.clamp((amount*q)/quantiles, 0, sorted.size()-1)));
					}
					List<Double> quantilesListRight = new ArrayList<>();
					for(int q=0;q<=quantiles;q++) {
						quantilesListRight.add(sorted.get(MathUtils.clamp(sorted.size() - amount + (amount*q)/quantiles, 0, sorted.size()-1)));
					}
					statistics.put("quantiles-IQ000-IQ010", String.valueOf(CollectionUtils.join(quantilesListLeft,",","%.3f")));
					statistics.put("quantiles-IQ099-IQ100", String.valueOf(CollectionUtils.join(quantilesListRight,",","%.3f")));
					statistics.put("IQ_99", String.format("%.6f",sorted.get((sorted.size()*99)/100)));
				}
				if(type.startsWith("{")){
					statistics.put("distribution", CollectionUtils.join(categoryCounts.getMap(name)));
					statistics.put("histogram", CollectionUtils.join(categoryCounts.getMap(name).values()));
					statistics.put("histogram-labels", CollectionUtils.join(categoryCounts.getMap(name).keySet()));
				}
				Entry<String,Integer> support = countsFirstValue.getMap(name).entrySet().iterator().next();
				statistics.put("support-first-value", support.getKey() + ":" + support.getValue());
			}
			statisticsList.add(statistics);
		}
		return statisticsList;
	}
}
