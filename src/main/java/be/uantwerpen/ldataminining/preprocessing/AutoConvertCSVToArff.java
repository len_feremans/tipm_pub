package be.uantwerpen.ldataminining.preprocessing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import be.uantwerpen.ldataminining.model.Dataset;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.IOUtils.Encoding;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.ldataminining.utils.Utils;

public class AutoConvertCSVToArff {

	public static boolean VERBOSE = true;
	private static final double ALMOST_ALWAYS = 0.99;
	private static final double MAX_MISMATCH_COUNT = 10;
	private static final double OCCURS_ALWAYS = 0.999999;

	
	public File run(File input, boolean inferAlmost) throws Exception{
		File output = IOUtils.getFileWithDifferentExtension(input, "arff");
		File scriptFile = IOUtils.getFileWithDifferentExtension(input, "_autoconvert.script");
		//convert arff
		Dataset dataset = new Dataset();
		dataset.setName(input.getName());
		dataset.setFile(input);
		dataset.setPreProcessedFile(output);
		//default seperator is ',', try other options
		List<String> firstLines = IOUtils.readFile(input, Encoding.DEFAULT, 3);
		char[] candidateSeperators = new char[]{';',','};
		char seperator = ',';
		for(char sep: candidateSeperators) {
			if(firstLines.get(0).contains("" + sep) && firstLines.get(1).contains("" + sep) ) {
				seperator = sep;
				break;
			}
		}
		dataset.setSeperator(seperator);
		AutoConvertCSVToArff autoConverter = new AutoConvertCSVToArff();
		autoConverter.convertCSVToArff(dataset, inferAlmost);
		autoConverter.makeMismatchUpdateScript(input, scriptFile);
		if(scriptFile.exists()){
			new ApplyRules().applyRules(scriptFile, output, output);
			scriptFile.delete();
		}
		return output;
	}
	
	
	public void makeMismatchUpdateScript(File inputFile, File outputFile) throws Exception{
		List<String> lines = new ArrayList<String>();
		Set<String> attributesWithIssues = new HashSet<String>();
		attributesWithIssues.addAll(this.illegalValues.keySet());
		attributesWithIssues.addAll(this.inferedNulls.keySet());
		for(String attribute: attributesWithIssues){
			lines.add(String.format("Actions %s", attribute));
			if(!this.illegalValues.get(attribute).isEmpty() && this.illegalValues.get(attribute).size() < MAX_MISMATCH_COUNT)
				for(String value: this.illegalValues.get(attribute)){
					lines.add(String.format("   removeIllegalValue(%s)", value));
				}
			if(this.inferedNulls.get(attribute) != null){
				lines.add(String.format("   setNull(%s)", this.inferedNulls.get(attribute)));
			}
		}
		if(lines.size() > 0){
			IOUtils.saveFile(lines, outputFile);
		}
		else{
			System.out.println("No mismatches found...");
		}
	}
	
	private boolean inferAlmost = true;
	
	/**
	 * Assumes columnLabels has labels attributes, or if null, attributes are in first row
	 * Converts CSV to ARFF. 
	 * 
	 * Note: To infer basic types (string/int/float/categorical) for each attribute, Table.inferColumnType is used,
	 * with 'fuzzy' rule if |distinct(values-attributes)| < 20 -> categorical else string.
	 * 
	 * TODO: Re-factor... just take regular arguments...
	 * @param inputCSV
	 * @param outputArff
	 * @param columnLabels
	 * @throws Exception
	 */
	public void convertCSVToArff(Dataset set, boolean inferAlmost) throws Exception
	{
		this.inferAlmost = inferAlmost;
		Timer timer = new Timer("convertCSVToArff");
		assert(set.getFile() != null && set.getFile().exists());
		assert(set.getPreProcessedFile() != null && set.getPreProcessedFile().getName().toLowerCase().endsWith(".arff"));
		//load file
		Table table = CSVUtils.loadCSV(set.getFile(), set.getSeperator(), set.getQuote());
		StringBuffer buff = new StringBuffer();
		buff.append("@relation " + IOUtils.getFilenameNoExtension(set.getFile()) + "\n\n");
		if(set.getColumnLabels() == null)
		{
			List<String> firstRow = table.dropFirst();
			if(VERBOSE)
				System.out.println("Warning: Asumming columnLabels are first row");
			set.setColumnLabels( firstRow.toArray(new String[firstRow.size()]));
		}
		//make header
		int idx = 0;
		int noDistinctValuesAllAttributes = 0;
		int noNotNominalAttributes = 0;
		int noClassValues = 0;
		Timer timer2 = new Timer("convertCSVToArff: infering columns");
		for(String column: set.getColumnLabels())
		{
			String escapedName = Utils.escapeName(column);

			String nullValue = InferAbnormalFrequentValues.inferNullValueEncoding(table.getColumnValues(idx));
			if(nullValue != null){
				if(VERBOSE)
					System.out.format("Infering nullValue %s: %s. Replacing with ? \n", escapedName, nullValue);
				inferedNulls.put(escapedName, nullValue);
				table.columnFindAndReplace(idx, nullValue,"?");
			}
			//get distinct values per column, see Table.getDistinctColumnValues
			int totalNotNull = table.getColumnCountNotNull(idx);
			Set<String> distinctColumnValues = table.getDistinctColumnValues(idx);
			distinctColumnValues.remove("?");

			String type = inferColumnType(escapedName, distinctColumnValues, totalNotNull);
			if(VERBOSE)
				System.out.format("Infering type %s: %s\n", escapedName, type);
			//inter type
			if(type.equals("categorical") || type.equals("boolean"))
			{
				Set<String> strings = new TreeSet<String>();
				for(Object obj: distinctColumnValues){
					String s = (String)obj;
					s = CSVUtils.escapeValueForArff(s);
					strings.add(s);
				}
				List<String> sortedDistinctColumnsValues = CollectionUtils.sorted(strings);
				buff.append(String.format("@attribute %s {%s}\n", escapedName, CollectionUtils.join(sortedDistinctColumnsValues)));
				if(column.equals("class"))
				{
					noClassValues = distinctColumnValues.size();
				}
				else
				{
					noDistinctValuesAllAttributes += distinctColumnValues.size();
				}
			}
			else if(type.startsWith("integer"))
			{
				String comment = Utils.substringAfter(type, "integer");
				buff.append(String.format("@attribute %s INTEGER %s\n", escapedName, comment));
				noNotNominalAttributes +=1;

			}
			else if(type.equals("float"))
			{
				String comment = Utils.substringAfter(type, "float");
				buff.append(String.format("@attribute %s REAL %s\n", escapedName, comment));
				noNotNominalAttributes +=1;
			}
			else if(type.startsWith("date"))
			{
				String format = Utils.substringAfter(type, "date ");
				String comment = "";
				if(format.contains("#")){
					format = Utils.substringBefore(format, "#").trim();
					comment = "#" + Utils.substringAfter(type, "#");
				}
				buff.append(String.format("@attribute %s DATE \"%s\" %s\n", escapedName, format, comment));
				noNotNominalAttributes +=1; 
			}
			else if(type.equals("string"))
			{
				buff.append(String.format("@attribute %s STRING\n", escapedName));
				noNotNominalAttributes +=1;
			}
			else if(type.startsWith("float #matches"))
			{
				String comment = Utils.substringAfter(type, "float"); //comments not supported otherwise by parser
				buff.append(String.format("@attribute %s REAL %s\n", escapedName, ""));
				noNotNominalAttributes +=1;
			}
			else if(type.startsWith("integer #matches"))
			{
				String comment = Utils.substringAfter(type, "integer"); //comments not supported otherwise by parser
				buff.append(String.format("@attribute %s INTEGER %s\n", escapedName, ""));
				noNotNominalAttributes +=1;
			}
			else if(type.startsWith("date #matches"))
			{
				String format = Utils.substringAfter(type, "date ");
				String comment = "";
				if(format.contains("#")){
					format = Utils.substringBefore(format, "#").trim();
					comment = "#" + Utils.substringAfter(type, "#");
				}
				buff.append(String.format("@attribute %s DATE \"%s\" %s\n", escapedName, format, "")); //comments not supported otherwise by parser
				noNotNominalAttributes +=1; 
			}
			else {
				buff.append(String.format("@attribute %s STRING\n", escapedName));
				noNotNominalAttributes +=1;
			}
			idx++;
		}	
		buff.append("\n");
		timer2.end();
		buff.append("@data\n");
		//add data
		if(!set.getPreProcessedFile().getParentFile().exists())
			set.getPreProcessedFile().getParentFile().mkdirs();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(set.getPreProcessedFile()), "UTF-8"));
		writer.append(buff.toString());
		buff = null;
		for(List<String> row: table.getRows())
		{
			writer.append(CSVUtils.serializeLineArff(row));
			if(row != table.getRows().get(table.getRows().size()-1))
				writer.append("\n");
		}
		writer.close();
		//print summary
		if(VERBOSE)
			System.out.format("Dataset: %s, Rows: %d, Colums nominal: %d, Columns continuous: %d, Values: %d, No classes: %s No, Total distinct nominal values: %d\n", 
				set.getName(), 
				table.size(), set.getColumnLabels().length - noNotNominalAttributes, noNotNominalAttributes, 
				set.getColumnLabels().length * table.size(), 
				noClassValues, noDistinctValuesAllAttributes);
		timer.end();
	}


	public abstract class PredicateFunction{
		public String name;
		public PredicateFunction(String s){
			this.name = s;
		}
		public abstract boolean isType(String value);
	}


	/**
	 * If all 0/1 or T/F or v1/v2 -> boolean
	 * If number of distinct objects < 30 -> categorical (even if integer)
	 * If all integers -> integer
	 * If all floats -> float
	 * else string
	 * 
	 * parameter: use getDistinctColumnValues(columnIdx)
	 * 
	 * @return
	 */
	private String inferColumnType(String attributeName, Set<String> distinctColumnValues, int totalNotNull)
	{
		distinctColumnValues.remove("?");
		//check boolean
		if(distinctColumnValues.size() <= 2){
			return "boolean";
		}
		//check categorical
		//if(distinctColumnValues.size() < 30){
		//;	return "categorical";
		//}
		int totalDistinct = distinctColumnValues.size();
		if(isEnumFuzzyCheck(totalNotNull, totalDistinct)){
			return "categorical";
		}
		//check date in common formats, see also
		//http://en.wikipedia.org/wiki/Date_format_by_country
		SimpleDateFormat[] formats = new SimpleDateFormat[]{
				//datetime
				new SimpleDateFormat("ddMMMyyyy:HH:mm:ss"),  
				new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"),  
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"),  
				//only date
				new SimpleDateFormat("dd/mm/yyyy"),  //belgium
				new SimpleDateFormat("dd-mm-yyyy"), 
				new SimpleDateFormat("dd.mm.yyyy"),
				new SimpleDateFormat("mm/dd/yyyy"),  //america
				new SimpleDateFormat("mm-dd-yyyy"), 
				new SimpleDateFormat("mm.dd.yyyy"), 
				new SimpleDateFormat("yyyy-mm-dd"),  //germany
				new SimpleDateFormat("yyyy/mm/dd"),
				new SimpleDateFormat("yyyy.mm.dd"),
				new SimpleDateFormat("yyyy-dd-mm"),  //other
				new SimpleDateFormat("yyyy/dd/mm"),
				new SimpleDateFormat("yyyy.dd.mm"),
		};
		for(final SimpleDateFormat format: formats){
			double isDateSupport = isTypeWithHighSupport(attributeName, distinctColumnValues, new PredicateFunction("date " + format) {
				
				@Override
				public boolean isType(String value) {
					return Utils.isDate(value, format);
				}
			});
			if(isDateSupport > OCCURS_ALWAYS){
				return "date " + format.toPattern();
			}
			else if(isDateSupport > ALMOST_ALWAYS && this.illegalValues.get(attributeName).size() < MAX_MISMATCH_COUNT && inferAlmost){
				System.out.println("INFO: type almost matches, e.g. examples: " + mismatches);
				return "date " + format.toPattern() + " #matches " + isDateSupport;
			}
			else{
				this.illegalValues.removeAll(attributeName);
			}
		}
		//Date or int?
		String[] formats2 = new String[]{"ddmmyyyy", "mmddyyyy", "yyyyddmm","yyyymmdd"};
		for(final String format: formats2){
			double isDateSupport = isTypeWithHighSupport(attributeName, distinctColumnValues, new PredicateFunction("date " + format) {
				
				@Override
				public boolean isType(String value) {
					return Utils.isDateNotInteger(value,format);
				}
			});
			if(isDateSupport > OCCURS_ALWAYS){
				return "date " + format;
			}
			else if(isDateSupport > ALMOST_ALWAYS && this.illegalValues.get(attributeName).size() < MAX_MISMATCH_COUNT && inferAlmost){
				System.out.println("INFO: type almost matches, e.g. examples: " + mismatches);
				return "date " + format + " #matches " + isDateSupport;
			}
			else{
				this.illegalValues.removeAll(attributeName);
			}
		}
		//check int
		double isIntegerSupport = isTypeWithHighSupport(attributeName, distinctColumnValues, new PredicateFunction("integer") {
			@Override
			public boolean isType(String value) {
				return Utils.isInteger(value);
			}
		});
		if(isIntegerSupport > OCCURS_ALWAYS){
			return "integer";
		}
		else if(isIntegerSupport > ALMOST_ALWAYS && this.illegalValues.get(attributeName).size() < MAX_MISMATCH_COUNT && inferAlmost){
			if(VERBOSE)
				System.out.println("INFO: type almost matches, e.g. examples: " + mismatches);
			return "integer #matches "+ isIntegerSupport;
		}
		else{
			this.illegalValues.removeAll(attributeName);
		}
		//check float
		double isFloatSupport = isTypeWithHighSupport(attributeName, distinctColumnValues, new PredicateFunction("float") {
			@Override
			public boolean isType(String value) {
				return Utils.isFloat(value);
			}
		});
		if(isFloatSupport > OCCURS_ALWAYS){
			return "float";
		}
		else if(isFloatSupport > ALMOST_ALWAYS && this.illegalValues.get(attributeName).size() < MAX_MISMATCH_COUNT && inferAlmost){
			if(VERBOSE)
				System.out.println("INFO: type almost matches, e.g. examples: " + mismatches);
			return "float #matches "+ isFloatSupport;
		}
		else{
			this.illegalValues.removeAll(attributeName);
		}
		//else
		return "string";
	}

	//new rule: if distinct valiues is less then 2% of total values and total distinct < 20 -> categorical
	public boolean isEnumFuzzyCheck(int totalNotNull, int totalDistinct) {
		boolean result1 = totalDistinct/(double)totalNotNull < 0.02 && totalDistinct < 20; //changed from 300 to 20!
		boolean result2= (totalDistinct < 10 && totalNotNull > 100);
		return result1 || result2;
	}

	private List<String> mismatches;
	private Multimap<String, String> illegalValues = ArrayListMultimap.create();
	private Map<String, String> inferedNulls = new HashMap<String,String>();
	
	public double isTypeWithHighSupport(String attribute, Collection<String> notNullValues, PredicateFunction function){
		int mismatch = 0;
		int match = 0;
		int idx = 0;
		mismatches = new ArrayList<String>();
		String nullInfered = this.inferedNulls.get(attribute);
 		for(String value: notNullValues){
 			if(nullInfered!= null && value.equals(nullInfered))
 				continue;
			if(function.isType(value)){
				match++;
			}
			else{
				mismatch++;
				mismatches.add(value);
			}
			idx++;
			if(idx == 40){
				if(mismatch > match)
					return 0.0;
			}
			if(idx == 1000){
				if(mismatch > 100)
					return match /(double) (mismatch + match);
			}
		}
		if(mismatch == 0.0){
			return 1.0;
		}
		else{
			double percentage =  match /(double) (mismatch + match);
			illegalValues.removeAll(attribute);
			illegalValues.putAll(attribute, mismatches);
			if(percentage > ALMOST_ALWAYS && mismatches.size() < MAX_MISMATCH_COUNT && inferAlmost){
				;
			}
			else if(percentage > 0.6){
				if(VERBOSE)
					System.err.format("Warning: Attribute %s. No inference of type %s. Support is %.3f\n", attribute, function.name, percentage);
			}
			return percentage;
		}
	}

}
