package be.uantwerpen.ldataminining.preprocessing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import be.uantwerpen.ldataminining.utils.Utils;

public class ApplyStructureUtils {

	public static  void dropZeroInformationAttributes(File input, File output) throws Exception {
		long rows = ArffUtils.getNumberOfRows(input);
		dropZeroInformationAttributes(input,output,(int)rows);
	}
	/**
	 * Business rules:
	 * 1) if < 0.001 of values is not null -> Drop attribute
	 * 2) if only one value possible, and almost never null -> Drop attribute
	 * 3) if many values possible, but one value almost always filled in -> Drop attribute
	 * 
	 * @param input
	 * @throws Exception
	 */
	@Deprecated //could be much faster, see ArffStatistics.getAllAttributeStatisticsFast
	public static  void dropZeroInformationAttributes(File input, File output, int sampleSize) throws Exception {
		List<List<String>> matrix = ArffUtils.loadDataMatrixSample(input, sampleSize);
		List<Map<String,String>> attributeStats = ArffStatistics.getAllAttributeStatisticsFast(input);
		final List<String> dropColumns = new ArrayList<String>();
		new ArffUtils();
		List<String> types = ArffUtils.getAttributeValues(input);
		new ArffUtils();
		List<String> allNames = ArffUtils.getAttributeNames(input);
		for(Map<String,String> stats: attributeStats){
			int notNull = Integer.valueOf(stats.get("noNotNull"));
			int total =  Integer.valueOf(stats.get("noValues"));
			double percentage = notNull / (double) total;
			if(percentage < 0.001) //only 243 values filled in
			{
				System.out.format("Dropping %s. Not null=%d.\n", stats.get("attribute"), notNull);
				dropColumns.add(stats.get("attribute"));
			}
		}
		for(Map<String,String> stats: attributeStats){
			int notNull = Integer.valueOf(stats.get("noNotNull"));
			int total =  Integer.valueOf(stats.get("noValues"));
			double percentage = notNull / (double) total;
			String attribute = stats.get("attribute");
			String type = types.get(allNames.indexOf(attribute));
			if(type.startsWith("{")){
				type = Utils.substringBetween(type, "{", "}");
				String[] valuesType = type.split("\\s?,\\s?");
				if(valuesType.length == 1 && percentage > 0.999) //always same value filled in
				{
					System.out.format("Dropping %s. Values: %s. Not null=%.3f.\n", attribute, type, percentage);
					dropColumns.add(stats.get("attribute"));
				}
				else if(valuesType.length > 1) //always same value filled in
				{
					//check null value again
					List<String> values = ArffUtils.getColumn(matrix, allNames.indexOf(attribute)); //ACTUALLY NOT NEEDED....
					values = ArffUtils.filterNotNull(values);
					//count support first value
					int supportFirstValue = 0;
					String firstValue = valuesType[0];
					for(String val: values){
						if(val.equals(firstValue)){
							supportFirstValue++;
						}
					}
					if(supportFirstValue > 0.999 * total){
						System.out.format("Dropping %s. Value %s has support of %d.\n", attribute, firstValue, supportFirstValue);
						dropColumns.add(stats.get("attribute"));
					}
					else{
						//System.out.format("Info: Not dropping %s. Value %s has support of %d.\n", attribute, firstValue, supportFirstValue);
					}
				}
			}
		}
		
		ArffTransforms.filterAttributes(input, output, new ArffTransforms.AttributeFilter() {
			
			public boolean keepAttribute(String attributeName) {
				return !dropColumns.contains(attributeName);
			}
		});
		System.out.format("Removed %d attributes\n", dropColumns.size());
	}
	
	/**
	 * 	Algorithm:
		for each 'prefix' attribute (that defines group of attributes):
			for each feature 
				copy features to schema(Original_table_unique_key, Group_key_(or prefix), Group_attributes)
	 * @param input
	 * @param keyAttribute
	 * @param groups
	 * @param attributes_in_groups
	 * @param outputCSV
	 * @throws Exception
	 */
	public static void splitDataForGroups(File input, String keyAttribute, String[] groups, String[] attributes_in_groups, File outputCSV) throws Exception{
		new ArffUtils();
		List<List<String>> dataMatrix = ArffUtils.loadDataMatrix(input);
		new ArffUtils();
		List<String> names = ArffUtils.getAttributeNames(input);
		List<List<String>> N_products_Output = new ArrayList<List<String>>();
		int colIdxKey = names.indexOf(keyAttribute);
		for(List<String> row: dataMatrix){
			for(String product_prefix: groups){
				List<String> rowProduct = new ArrayList<String>();
				rowProduct.add(row.get(colIdxKey));
				rowProduct.add(product_prefix);
				boolean nonEmptyProduct = false;
				for(String product_feature: attributes_in_groups){
					int sourceColIdx = names.indexOf(product_prefix + "_" + product_feature);
					String value = row.get(sourceColIdx);
					if(value.equals("0") || value.equals("?") || value.equals("31/12/6999"))
						value = "?";
					else{
						nonEmptyProduct = true;
					}
					rowProduct.add(value);
				}
				if(nonEmptyProduct)
					N_products_Output.add(rowProduct);
			}
		}
		//save
		List<String> header = new ArrayList<String>(Arrays.asList(attributes_in_groups));
		header.add(0, "product");
		header.add(0, keyAttribute);
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputCSV));
		writer.write(CSVUtils.serializeLineCSV(header));
		writer.newLine();
		for(List<String> row: N_products_Output){
			writer.write(CSVUtils.serializeLineCSV(row));
			writer.newLine();
		}
		System.out.println("Saved " + N_products_Output.size() + " rows.");
		writer.flush();
		writer.close();
	}
	
}
