package be.uantwerpen.ldataminining.preprocessing;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.uantwerpen.ldataminining.model.CountMap;
import be.uantwerpen.ldataminining.utils.CollectionUtils;

public class FindNullPattern {
	//e.g. if always c1=Y,a1=X,a2=X,b1=?,b2=?
	//		or c1=Y,b1=X,b2=X
	//-> return this, e.g. <<c1,a1,a2>,10>,<<c1,b1,b2>,5>> 
	public static List<List<String>>  findNullPattern(File arffInput, int amount) throws Exception{
		List<List<String>> returnList = new ArrayList<List<String>>();
		try {
			List<String> cols = ArffUtils.getAttributeNames(arffInput);
			List<List<String>> rows = ArffUtils.loadDataMatrix(arffInput);
			CountMap<String> nullPatternsCount = new CountMap<>();
			for(List<String> row: rows) {
				String nullpattern = "";
				for(int i=0; i<row.size(); i++) {
					if(!row.get(i).equals("?")) {
						nullpattern += "1";
					}
					else {
						nullpattern += "0";
					}
				}
				nullPatternsCount.add(nullpattern);
			}
			List<String> nullPatterns = nullPatternsCount.getKeysSortedDescending();
			int maxReport = amount;

			for(String nullPattern: nullPatterns.subList(0, Math.min(maxReport,nullPatterns.size()))) {
				int support = nullPatternsCount.get(nullPattern);
				List<String> attributesNotNull = new ArrayList<String>();
				for(int i=0; i<nullPattern.length(); i++) {
					if(nullPattern.charAt(i) == '1') {
						if(i < cols.size())
							attributesNotNull.add(cols.get(i));
						else {
							System.err.println("Error renders nullpattern results " + nullPattern + " cols: " + cols);
						}
					}
				}
				returnList.add(Arrays.asList(new String[] {CollectionUtils.join(attributesNotNull,", "), String.valueOf(support)}));
			}
			System.out.println("nullpattern:" + CollectionUtils.join(returnList));
		}catch(Exception e) {
			System.err.println("Error computing nullpatterns. " + e.getMessage());
			throw new RuntimeException(e);
		}
		return returnList;
	}
}
