package be.uantwerpen.ldataminining.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SetMap<K,V> {

	private Map<K,Set<V>> map = new HashMap<K,Set<V>>();
	
	public void put(K key, V value){
		Set<V> set = map.get(key);
		if(set == null){
			set = new HashSet<V>();
			map.put(key, set);
		}
		set.add(value);
	}
	
	public void remove(K key){
		map.remove(key);
	}
	
	public Set<V> get(K key){
		return map.get(key);
	}
	
	public Set<K> keySet(){
		return map.keySet();
	}
}
