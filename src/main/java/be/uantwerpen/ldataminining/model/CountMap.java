package be.uantwerpen.ldataminining.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class CountMap<K extends Comparable> {

	private Map<K,Integer> map;
	
	public CountMap() {
		this(false);
	}
	
	public CountMap(boolean sorted) {
		if(sorted) {
			map = new TreeMap<K,Integer>();
		}
		else {
			map = new HashMap<K,Integer>(1000);
		}
	}
	
	
	public void clear(){
		map.clear();
	}
	
	public void add(K key){
		map.put(key, get(key) + 1);
	}
	
	public int get(K key){
		Integer i = map.get(key);
		return (i == null)?0:i;
	}
	
	public Map<K,Integer> getMap(){
		return map;
	}
	
	public Set<K> keySet(){
		return map.keySet();
	}
	
	public Integer computeMedian(){
		List<Integer> lst = new ArrayList<Integer>(map.values());
		Collections.sort(lst);
		return lst.get(lst.size()/2);
	}
	
	public List<K> getKeysSortedDescending(){
		List<Entry<K,Integer>> lst = new ArrayList<>(map.entrySet());
		List<K> keysSorted = lst.stream().
						sorted((entry1,entry2) -> entry2.getValue().compareTo(entry1.getValue())).
						map(entry -> entry.getKey()).collect(Collectors.toList());
		return keysSorted;
	}
	
	public String toString(){
		StringBuffer buff = new StringBuffer();
		List<Entry<K,Integer>> sorted = new ArrayList<Entry<K,Integer>>(map.entrySet());
		Collections.sort(sorted, new Comparator<Entry<K,Integer>>(){

			@Override
			public int compare(Entry<K, Integer> o1, Entry<K, Integer> o2) {
				return o1.getValue() - o2.getValue();
			}
			
		});
		if(sorted.size() < 20)
			for(Entry<K,Integer> entry: sorted){
				buff.append(entry.getKey() + ":" + entry.getValue() + "\n");
			}
		else{
			for(Entry<K,Integer> entry: sorted.subList(0, 10)){
				buff.append(entry.getKey() + ":" + entry.getValue() + "\n");
			}
			buff.append("...\n");
			for(Entry<K,Integer> entry: sorted.subList(sorted.size()-10, sorted.size())){
				buff.append(entry.getKey() + ":" + entry.getValue() + "\n");
			}
		}
		return buff.toString();
	}
}
