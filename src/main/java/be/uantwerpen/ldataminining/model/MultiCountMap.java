package be.uantwerpen.ldataminining.model;

import java.util.Map;

public class MultiCountMap<K1,K2>{

	private MultiKeyMap2<K1, K2, Integer> map = new MultiKeyMap2<K1, K2, Integer>();
	
	public void add(K1 key1, K2 key2){
		Integer count = map.get(key1,key2);
		count = count == null? 1: count+1;
		map.put(key1, key2, count);
	}
	
	public void put(K1 key1, K2 key2, int count){
		map.put(key1, key2, count);
	}
	
	
	public int get(K1 key1, K2 key2){
		Integer i = map.get(key1, key2);
		return (i == null)?0:i;
	}
	
	public Map<K2,Integer> getMap(K1 key1){
		return map.get(key1);
	}
	
}
