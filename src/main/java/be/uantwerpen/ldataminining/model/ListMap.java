package be.uantwerpen.ldataminining.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class ListMap<K,V> {

	private Map<K,List<V>> map;
	
	public ListMap() {
		map = new HashMap<K,List<V>>();
	}
	
	public ListMap(boolean sorted) {
		if(sorted)
			map = new TreeMap<K,List<V>>();
		else
			map = new HashMap<K,List<V>>();
	}
	
	public void put(K key, V value){
		List<V> lst = map.get(key);
		if(lst == null){
			lst = new ArrayList<V>();
			map.put(key, lst);
		}
		lst.add(value);
	}
	
	public void putAll(K key, List<V> values){
		List<V> lst = map.get(key);
		if(lst == null){
			lst = values;
			map.put(key, lst);
		}
		lst.addAll(values);
	}
	
	public void putList(K key, List<V> lst){
		map.put(key, lst);
	}
	
	
	public void remove(K key){
		map.remove(key);
	}
	
	public List<V> get(K key){
		return map.get(key);
	}
	
	public Set<K> keySet(){
		return map.keySet();
	}

	public Set<Entry<K,List<V>>> entrySet() {
		return map.entrySet();
	}
	
	public Set<V> values() {
		Collection<List<V>> allVals = map.values();
		Set<V> values = new HashSet<V>();
		for(List<V> valuesL: allVals) {
			values.addAll(valuesL);
		}
		return values;
	}
	
	public String toString(){
		return map.toString();
	}
}
