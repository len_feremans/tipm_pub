package be.uantwerpen.mime_webapp.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.mime_webapp.Settings;


public class FileItem implements Serializable{
	private static final long serialVersionUID = -5141658337025657977L;

	private String logicalName;
	private int version=0;
	private String id;
	
	private List<String> stackOperations = new ArrayList<String>();
	private String filename;
	
	private Long noColumns;
	private Long noRows;
	
	private List<PatternSet> patterns = new ArrayList<PatternSet>();
	private List<AnomalyScores> scores = new ArrayList<AnomalyScores>();
    
	//back link to project, give 'infinite' loop error when saving
 
    public FileItem() {}
    
    public FileItem clone() {
    		FileItem item = new FileItem();
    		item.setNoRows(getNoRows());
		item.setNoColumns(getNoColumns());
		item.setLogicalName(getLogicalName());
		item.setFilename(getFilename());
		item.setVersion(getVersion()+1);
		item.setStackOperations(new ArrayList<String>(getStackOperations()));
		item.setPatterns(new ArrayList<>(getPatterns()));
		item.setScores(new ArrayList<AnomalyScores>());
		item.getId();
		return item;
    }

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getLogicalName() {
		return logicalName;
	}

	public void setLogicalName(String logicalName) {
		this.logicalName = logicalName;
	}

	public Long getNoColumns() {
		return noColumns;
	}

	public void setNoColumns(Long noColumns) {
		this.noColumns = noColumns;
	}

	public Long getNoRows() {
		return noRows;
	}

	public void setNoRows(Long noRows) {
		this.noRows = noRows;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<String> getStackOperations() {
		return stackOperations;
	}

	public void setStackOperations(List<String> stackOperations) {
		this.stackOperations = stackOperations;
	}
	
	public List<PatternSet> getPatterns() {
		return patterns;
	}

	public void setPatterns(List<PatternSet> patterns) {
		this.patterns = patterns;
	}

	//methods:
	public void add(PatternSet set) {
		//if pattern set with same algorithm and input columns, and higher support -> remove pattern set
		for(PatternSet set2: this.patterns) {
			if(set2.getAlgorithm().equals(set.getAlgorithm()) 
				&& set2.getColumns().equals(set.getColumns())
				&& Integer.valueOf(set2.getSupport()).compareTo(Integer.valueOf(set.getSupport())) > 0){
				this.patterns.remove(set2);
				break;
			}
		}
		this.patterns.add(set);
	}
	
	public List<AnomalyScores> getScores() {
		return scores;
	}

	public void setScores(List<AnomalyScores> scores) {
		this.scores = scores;
	}

	public boolean isArff(){
		return this.filename.toLowerCase().endsWith(".arff");
	}
	
	public boolean isCSV(){
		return this.filename.toLowerCase().endsWith(".csv");
	}
	
	public String getId(){ //globally unique...
		this.id = String.format("%s-%03d", logicalName, version); 
		return id; //e.g. iris-001
	}

	public File getFile(){
		return new File(Settings.FILE_FOLDER, this.filename);
	}

	@Override
    public String toString() {
        return String.format(
                "Item[id='%s']",  getId());
    }
	
	public File generateNewOutputFilename(){
		return new File(Settings.FILE_FOLDER, String.format("%s-%03d.%s", logicalName, version+1, IOUtils.getExtension(getFile())));
	}
	
	public File generateNewOutputFilename(int next_version){
		return new File(Settings.FILE_FOLDER, String.format("%s-%03d.%s", logicalName, next_version, IOUtils.getExtension(getFile())));
	}

	@Override
	public boolean equals(Object obj) {
		return getId().equals(((FileItem)obj).getId());
	}
	

}