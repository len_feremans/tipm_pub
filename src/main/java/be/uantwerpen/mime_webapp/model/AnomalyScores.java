package be.uantwerpen.mime_webapp.model;

import java.util.List;
import java.util.Map;

public class AnomalyScores {
	private String label; //e.g. FPOF(maximal-itemsets-pc1, sequential-patterns-pc1)
	private String filename;
	private List<String> filenamesPatternSets;
	private String algorithm;
	private Map<String,Double> evaluationResults;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public List<String> getFilenamesPatternSets() {
		return filenamesPatternSets;
	}
	public void setFilenamesPatternSets(List<String> filenamesPatternSets) {
		this.filenamesPatternSets = filenamesPatternSets;
	}
	public String getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	public Map<String, Double> getEvaluationResults() {
		return evaluationResults;
	}
	public void setEvaluationResults(Map<String, Double> evaluationResults) {
		this.evaluationResults = evaluationResults;
	}
	
	
}
