package be.uantwerpen.mime_webapp.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonManagedReference;


public class Project implements Serializable{

	private static final long serialVersionUID = 399579716649232694L;

    private String name;
    
    @JsonManagedReference("items")
    private ArrayList<FileItem> items = new ArrayList<FileItem>();
 
    public Project() {}

    public Project(String name) {
        this.name = name;
    }
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<FileItem> getFileItems() {
		return items;
	}

	public void setFileItemsItems(ArrayList<FileItem> items) {
		this.items = items;
	}
	
	public void add(FileItem item)
	{	
		item.getId();
		this.items.add(item);
	}
	
	public void remove(FileItem item)
	{
		this.items.remove(item);
		File file = item.getFile();
		if(file.exists()){
			file.delete();
			System.err.println("Deleting file " + file.getAbsolutePath());
		}
		return;
	}

	@Override
	public boolean equals(Object obj) {
		return this.name.equals(((Project)obj).name);
	}

	@Override
    public String toString() {
        return String.format("Project[name='%s']", name);
    }

	

}