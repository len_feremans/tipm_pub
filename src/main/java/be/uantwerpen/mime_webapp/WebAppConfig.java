package be.uantwerpen.mime_webapp;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/**
 * Documentation links:
Spring:
X Run helloworld web-app: See http://spring.io/guides/gs/spring-boot/
X CRUD programming using REST: See http://wimdeblauwe.wordpress.com/2013/12/16/creating-a-rest-web-application-in-4-classes/
X Upload files: See http://spring.io/guides/gs/uploading-files/
X Basic Spring application (with JDBC): http://www.codejava.net/frameworks/spring/spring-mvc-with-jdbctemplate-example
X MVC details: http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html
X App configuration for MVC details: http://www.luckyryan.com/2013/02/07/migrate-spring-mvc-servlet-xml-to-java-config/

 * @author lfereman
 *
 */
@Configuration
@EnableAutoConfiguration
@EnableWebMvc
@ComponentScan
public class WebAppConfig extends WebMvcConfigurerAdapter {

	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
		"classpath:/WEB-INF/resources/","classpath:/WEB-INF/resources/", 
		"classpath:/META-INF/resources/", 
		"classpath:/resources/",
		"classpath:/resources/css",
		"classpath:/resources/img",
		"/resources",
        "classpath:/static/", 
        "classpath:/public/" };

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/css/**").addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
		registry.addResourceHandler("/img/**").addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
		registry.addResourceHandler("/js/**"). addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/list").setViewName("list");
		registry.addViewController("/data").setViewName("data");
		registry.addViewController("/transform").setViewName("transform");
		registry.addViewController("/plot").setViewName("plot");
		registry.addViewController("/query").setViewName("query");
		registry.addViewController("/mine").setViewName("mine");
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}   

	@Bean
	public ViewResolver getViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
		//InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	    //viewResolver.setPrefix("/WEB-INF/views/");
	    //viewResolver.setSuffix(".jsp");
	    //viewResolver.setViewClass(JstlView.class);
	    	//return viewResolver;
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("2200800KB");
		factory.setMaxRequestSize("2200800KB");
		return factory.createMultipartConfig();
	}

	public static void main(String[] args) {
		SpringApplication.run(WebAppConfig.class, args);

		//System.out.println("Let's inspect the beans provided by Spring Boot:");
		//String[] beanNames = ctx.getBeanDefinitionNames();
		//Arrays.sort(beanNames);
		//for (String beanName : beanNames) {
		//	System.out.println(beanName);
		//}
	}
}
