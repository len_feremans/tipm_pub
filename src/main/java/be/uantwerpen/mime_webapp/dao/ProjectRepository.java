package be.uantwerpen.mime_webapp.dao;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.thoughtworks.xstream.XStream;

import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.PatternSet;
import be.uantwerpen.mime_webapp.model.Project;
import static java.util.Comparator.*;

//Singleton/ Data Accessor pattern
@Service
public class ProjectRepository {

	private static File datafile = new File(Settings.DATA_FILE); //no caching for know

	private List<Project> projects = new ArrayList<Project>();

	public Project findByName(String name){
		load();
		for(Project proj: this.projects){
			if(proj.getName().equalsIgnoreCase(name))
				return proj;
		}
		return null;
	}

	public Project saveProject(Project projectEntity){
		System.out.println("dao>>saveProject(" + projectEntity.getName() + ")");
		load();
		this.projects.remove(projectEntity); //why? remove old 'version' using equals (based on name0
		for(FileItem item: projectEntity.getFileItems())
			item.getId();
		this.projects.add(projectEntity);
		save();
		return projectEntity;
	}

	public void removeProject(String name){
		this.projects.remove(new Project(name));
		save();
	}

	public List<Project> findAll(){
		load();
		return projects;
	}

	public FileItem findItemById(String itemId){
		for(Project project: projects){
			for(FileItem item: project.getFileItems()){
				if(item.getId().equals(itemId))
					return item;
			}
		}
		return null;
	}
	
	public Integer findNextVersion(String logicalName){
		int maxVersion = 0;
		for(Project project: projects){
			for(FileItem item: project.getFileItems()){
				if(item.getLogicalName().equals(logicalName)) {
					maxVersion = Math.max(item.getVersion(),maxVersion);
				}
					
			}
		}
		return maxVersion+1;
	}
	
	public FileItem removeItem(String itemId) {
		for(Project project: projects){
			for(FileItem item: project.getFileItems()){
				if(item.getId().equals(itemId)){
					project.remove(item);
					save();
					return item;
				}
			}
		}
		return null;
	}
	
	//xml stuff to save/load
	@SuppressWarnings("unchecked")
	private void load(){
		if(!datafile.exists())
		{
			this.projects = new ArrayList<Project>();
			return;
		}
		try{
			XStream xstream = getSerializer();
			FileReader reader = new FileReader(datafile);
			this.projects = (List<Project>) xstream.fromXML(reader);
			reader.close();
			//inverse projects, so last added project is first
			Collections.reverse(this.projects);
			//Group by dataset name, within each project
			for(Project proj: this.projects) {
				List<FileItem> fileItems = proj.getFileItems();
				//see https://stackoverflow.com/questions/30382453/java-stream-sort-2-variables-ascending-desending
				fileItems = fileItems.stream().sorted(comparing(FileItem::getLogicalName)
										 .thenComparing(comparing(FileItem::getVersion).reversed()))
										 .collect(Collectors.toList());
				proj.setFileItemsItems(new ArrayList<FileItem>(fileItems));
			}
			System.out.println("dao>>Loading " + datafile.getName());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void save(){
		XStream xstream = getSerializer();
		try {
			if(!datafile.exists()) {
				datafile.getParentFile().mkdirs();
			}
			FileWriter writer = new FileWriter(datafile);
			xstream.toXML(this.projects, writer);
			writer.close();
			System.out.println("Saving " + datafile.getName());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private XStream getSerializer() {
		XStream xstream = new XStream();
		xstream.alias("project", Project.class);
		xstream.alias("item", FileItem.class);
		xstream.alias("patterns", PatternSet.class);
		return xstream;
	}

}
