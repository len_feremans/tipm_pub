package be.uantwerpen.mime_webapp;

public class Settings {
	public static String FILE_FOLDER = "./data/upload/";
	public static String DATA_FILE = "./data/projects.xml";
	public static String SPMF_JAR = "./lib/spmf.jar";
	public static String PBAD = "../pbad-public/";
	public static String PYTHON3 = "/usr/local/bin/python3";
}
