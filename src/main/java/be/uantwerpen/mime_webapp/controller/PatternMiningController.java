package be.uantwerpen.mime_webapp.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.datamining.pattern_mining.ArffToSPMF;
import be.uantwerpen.datamining.pattern_mining.MakePatternOccurrences;
import be.uantwerpen.datamining.pattern_mining.MakePatternOccurrencesSpan;
import be.uantwerpen.datamining.pattern_mining.MineUsingSPMF;
import be.uantwerpen.datamining.pattern_mining.RunPBADEmbeddingOnly;
import be.uantwerpen.ldataminining.model.CountMap;
import be.uantwerpen.ldataminining.model.ListMap;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.model.SetMap;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.CommandLineUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.ldataminining.utils.Triple;
import be.uantwerpen.ldataminining.utils.Utils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.AnomalyScores;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.MySession;
import be.uantwerpen.mime_webapp.model.PatternSet;
import be.uantwerpen.mime_webapp.model.Project;

@RestController
public class PatternMiningController {

	@Autowired
	ProjectRepository repository;

	MineUsingSPMF minerSPMF = new MineUsingSPMF();
	MakePatternOccurrences makePatternOccurrences = new MakePatternOccurrences();
	MakePatternOccurrencesSpan makePatternOccurrencesSpan = new MakePatternOccurrencesSpan();
	RunPBADEmbeddingOnly runPBADEmbeddingOnly = new RunPBADEmbeddingOnly();

	@RequestMapping(value="/rest/mining/run-itemsets", method=RequestMethod.POST)
	public @ResponseBody String runItemsetMining(
			@RequestParam("columns") String columns,
			@NotNull @RequestParam("algorithm") String algorithm,
			@NotNull @RequestParam("support") String support,
			HttpServletRequest request)
	{
		try {
			String[] columnsArr = columns.split(",\\s*");
			FileItem currentInput = getCurrentItem(request.getSession());
			if(!currentInput.isArff())
				throw new RuntimeException("Only Arff supported");
			File data  = currentInput.getFile();
			Double supportAsFloat = Double.valueOf(support);
			File outputReadable = minerSPMF.runItemsetMining(data, Arrays.asList(columnsArr), algorithm, supportAsFloat);
			savePatterns(request.getSession(), currentInput.getFile(), outputReadable, "itemsets", columns, algorithm, support);
			File occurrencesFile = storePatternSetOccurrences(outputReadable.getName(),request);
			savePatternsWithMetadata(outputReadable, occurrencesFile,currentInput.getFile());
			return String.format("Found %d patterns.",IOUtils.countLines(outputReadable)-1);
		}
		catch(InterruptedException e) {
			e.printStackTrace();
			throw(new RuntimeException(e.getMessage()));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw(new RuntimeException(e.getMessage()));
		}
	}
	
	@RequestMapping(value="/rest/mining/run-sp", method=RequestMethod.POST)
	public @ResponseBody String runSequentialPatternMining(
			@RequestParam("columns") String columns,
			@NotNull @RequestParam("algorithm") String algorithm,
			@NotNull @RequestParam("support") String support,
			HttpServletRequest request)
	{
		try {
			String[] columnsArr = columns.split(",\\s*");
			FileItem currentInput = getCurrentItem(request.getSession());
			if(!currentInput.isArff())
				throw new RuntimeException("Only Arff supported");
			File data  = currentInput.getFile();
			Double supportAsFloat = Double.valueOf(support);
			File outputReadable = minerSPMF.runSequentialPatternMining(data, Arrays.asList(columnsArr), algorithm, supportAsFloat);
			savePatterns(request.getSession(), currentInput.getFile(), outputReadable, "sequential patterns", columns, algorithm, support);	
			File occFile = storePatternSetOccurrences(outputReadable.getName(),request);
			savePatternsWithMetadata(outputReadable, occFile, currentInput.getFile());
			return String.format("Found %d patterns.",IOUtils.countLines(outputReadable)-1);
		}
		catch(InterruptedException e) {
			e.printStackTrace();
			throw(new RuntimeException(e.getMessage()));
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private File storePatternSetOccurrences(String patternFilename, HttpServletRequest request) throws Exception
	{
		//get current input
		MySession mySession = (MySession) request.getSession().getAttribute("mySession");
		Project projectObj = repository.findByName(mySession.getCurrentProject());
		FileItem currentInput = repository.findItemById(mySession.getCurrentItem());
		if(!currentInput.isArff())
			throw new RuntimeException("Only Arff supported");
		//get pattern set
		PatternSet set = null;
		for(PatternSet patternSet: currentInput.getPatterns()) {
			if(patternSet.getFilename().equals(patternFilename)) {
				set = patternSet;
			}
		}
		if(set == null) {
			throw new RuntimeException("Pattern set with name " + patternFilename + " not found!");
		}
		File arff  = currentInput.getFile();
		//save occurrences
		Table table = new Table();
		table.addRow(Arrays.asList("Window", "PatternId"));
		ListMap<Integer,Integer> occurrences = makePatternOccurrences.makeExactPatternOccurrences(arff, set);
		for(int window: occurrences.keySet()) {
			for(int patternId: occurrences.get(window)) {
				table.addRow(Arrays.asList(String.valueOf(window),String.valueOf(patternId)));
			}
		}
		File occurencesFile = new File(Settings.FILE_FOLDER + set.getFilename() + "_occurrences.csv");
		CSVUtils.saveTable(table, occurencesFile);
		//save projects.xml
		set.setFilenameOccurrences(occurencesFile.getName());
		repository.saveProject(projectObj);
		return occurencesFile;
	}

	private void savePatternsWithMetadata(File patternsFile, File occurrencesFile, File itemFile) throws Exception {
		//TODO: use RDBMS or something like pandas for Java
		//e.g. PatternsFile:
		//pattern,support
		//pc1=1 pc1=2 pc1=4 pc2=1 pc3=4 pc3=5,56
		//pc1=2 pc1=3 pc2=0 pc2=1 pc3=4 pc3=5 pc3=6,56
		Table patterns = CSVUtils.loadCSV(patternsFile);
		List<List<String>> patternsList = patterns.getRowsStartingFrom1();
		//OccurrencesFile: 
		//Window,PatternId (starting from 1)
		//1,978
		//2,20
		//2,94
		Table pattern2window  = CSVUtils.loadCSV(occurrencesFile);
		ListMap<Integer,Integer> patternId2windowListMap = new ListMap<>();
		for(List<String> row: pattern2window.getRowsStartingFrom1()) {
			patternId2windowListMap.put(Integer.valueOf(row.get(1)) -1, Integer.valueOf(row.get(0))); //patternId (-1 to get zero-based idx)->window1,window2...
		}
		//DataFile:
		Map<Integer, Integer> window2LabelMap = getWindowLabels(itemFile);
		//output: patterns with confidence, e.g. computing support normal, conf (normal/support) en 1-conf
		patterns.getRows().get(0).addAll(Arrays.asList("conf","windows","anomalies"));
		for(int patternId=0; patternId < patternsList.size(); patternId++) {
			List<String> patternRow =  patternsList.get(patternId);
			String pattern = patternRow.get(0);
			Integer support = Integer.valueOf(patternRow.get(1));
			List<Integer> windows = patternId2windowListMap.get(patternId);
			List<Integer> anomalies = new ArrayList<>();
			for(Integer window: windows) {
				int label = window2LabelMap.get(window);
				if(label == 1) {
					anomalies.add(window);
				}
			}
			//check
			if(windows.size() != support) {
				System.err.println("Warning pattern support != #occurrences " + pattern + " support:" + support + ", #windows:" + windows.size());
			}
			double conf = (windows.size() - anomalies.size()) / (double)windows.size(); //conf, that if X->label=normal
			patternRow.clear();
			patternRow.addAll(Arrays.asList(pattern, String.valueOf(windows.size()), String.valueOf(conf), CollectionUtils.join(windows, " "), CollectionUtils.join(anomalies, " ")));
		}
		CSVUtils.saveTable(patterns, new File(patternsFile.getAbsolutePath() + "_metadata.csv"));
	}

	private Map<Integer, Integer> getWindowLabels(File itemFile) throws IOException {
		// timestamp,value,label,Window
		Table window2label = MakePatternOccurrences.groupByWindow(itemFile,0,Integer.MAX_VALUE);
		Map<Integer,Integer> window2LabelMap = new HashMap<>();
		List<String> columns = window2label.getRows().get(0);
		int windowIdx = columns.indexOf("Window");
		int labelIdx = columns.indexOf("label");
		for(List<String> row: window2label.getRowsStartingFrom1()) {
			List<String> labels = Arrays.asList(row.get(labelIdx).split(";"));
			int label = -1;
			if(labels.contains("1.0")) {
				label = 1;
			}
			else if(labels.contains("-1.0")) {
				label = -1;
			}
			else {
				label = 0; //ok?
			}
			window2LabelMap.put(Integer.valueOf(row.get(windowIdx)), label); //window->label
		}
		return window2LabelMap;
	}
	
	@RequestMapping(value="/rest/mining/remove-patternset", method=RequestMethod.POST)
	public @ResponseBody String removePatternset(
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		try {
			MySession mySession = (MySession) request.getSession().getAttribute("mySession");
			FileItem current_item = repository.findItemById(mySession.getCurrentItem());
			for(PatternSet set: current_item.getPatterns()) {
				if(set.getFilename().equals(filename)) {
					current_item.getPatterns().remove(set);
					//remove files:
					File patternsFile = new File(Settings.FILE_FOLDER + set.getFilename());
					if(patternsFile.exists()) {
						patternsFile.delete();
						System.out.println("Deleting " + patternsFile.getName());
					}
					if(set.getFilenameOccurrences() != null && !set.getFilenameOccurrences().equals("")) {
						File occurrencesFile = new File(Settings.FILE_FOLDER + set.getFilenameOccurrences());
						if(occurrencesFile.exists()) {
							occurrencesFile.delete();
							System.out.println("Deleting " + occurrencesFile.getName());
						}
					}
					//update repository
					repository.save();
					break;
				}
			}
			return "Removed patterns.";
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@RequestMapping(value="/rest/mining/filter-length", method=RequestMethod.POST)
	public @ResponseBody void filterPatternsOnLength(
			@RequestParam("filename") String filename,
			@RequestParam("minlen") String minlen,
			@RequestParam("maxlen") String maxlen,
			HttpServletRequest request) throws Exception
	{
		//1. get input data
		MySession mySession = (MySession) request.getSession().getAttribute("mySession");
		FileItem current_item = repository.findItemById(mySession.getCurrentItem());
		Optional<PatternSet> patternSet = current_item.getPatterns().stream().filter((x) -> x.getFilename().equals(filename)).findFirst();
		if(!patternSet.isPresent())
			throw new RuntimeException("patternSet not found");
		int minlenInt = 1;
		int maxlenInt = Integer.MAX_VALUE;
		if(!minlen.isEmpty()) 
			minlenInt = Integer.parseInt(minlen);
		if(!maxlen.isEmpty())
			maxlenInt = Integer.parseInt(maxlen);
		//2. transform
		File patternFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilename());
		Table patternsTable = CSVUtils.loadCSV(patternFile);
		Table newTable = new Table();
		newTable.addRow(Arrays.asList("pattern","support"));
		//e.g. like 
		//pattern,support
		//value=9 value=10 value=9 value=8,23
		for(int i=1; i<patternsTable.getRows().size(); i++) {
			String pattern = patternsTable.getRows().get(i).get(0);
			String support = patternsTable.getRows().get(i).get(1);
			int lenPattern = pattern.split(" ").length;
			if(lenPattern >= minlenInt && lenPattern <= maxlenInt) {
				newTable.addRow(Arrays.asList(pattern,support));
			}
		}
		//3. save file + update stack operations + update occurrences
		CSVUtils.saveTable(newTable, patternFile);
		patternSet.get().setNoPatterns((long)newTable.getRows().size() -1);
		current_item.getStackOperations().add(String.format("Filter patterns(set=%s,minlen=%s,maxlen=%s)",
				patternSet.get().getLabel(),minlen,maxlen));
		repository.save();
		File occFile = storePatternSetOccurrences(patternFile.getName(),request);
		savePatternsWithMetadata(patternFile, occFile, current_item.getFile());
	}

	@RequestMapping(value="/rest/mining/filter-support", method=RequestMethod.POST)
	public @ResponseBody void filterPatternsOnSupport(
			@RequestParam("filename") String filename,
			@RequestParam("topk") String topk,
			HttpServletRequest request) throws Exception
	{
		//1. get input data
		MySession mySession = (MySession) request.getSession().getAttribute("mySession");
		FileItem current_item = repository.findItemById(mySession.getCurrentItem());
		Optional<PatternSet> patternSet = current_item.getPatterns().stream().filter((x) -> x.getFilename().equals(filename)).findFirst();
		if(!patternSet.isPresent())
			throw new RuntimeException("patternSet not found");
		int topkInt = Integer.parseInt(topk);
		//2. transform
		File patternFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilename());
		Table patternsTable = CSVUtils.loadCSV(patternFile);
		Table newTable = new Table();
		newTable.addRow(patternsTable.getRows().get(0));
		for(int i=1; i<patternsTable.getRows().size() && i<=topkInt; i++) {
			newTable.addRow(patternsTable.getRows().get(i));
		}
		//3. save file + update stack operations + update occurrences
		CSVUtils.saveTable(newTable, patternFile);
		patternSet.get().setNoPatterns((long)newTable.getRows().size() -1);
		current_item.getStackOperations().add(String.format("Filter support(set=%s,topK=%s)",
				patternSet.get().getLabel(),topk));
		repository.save();
		storePatternSetOccurrences(patternFile.getName(),request);
	}

	//todo: Now only WITHIN 1 patternset, not between patternsets!
	@RequestMapping(value="/rest/mining/filter-jaccard", method=RequestMethod.POST)
	public @ResponseBody void filterPatternsOnJaccardSimmilarity(
			@RequestParam("filename") String filename,
			@RequestParam("threshold") String threshold,
			HttpServletRequest request) throws Exception
	{
		//1. get input data
		MySession mySession = (MySession) request.getSession().getAttribute("mySession");
		FileItem current_item = repository.findItemById(mySession.getCurrentItem());
		Optional<PatternSet> patternSet = current_item.getPatterns().stream().filter((x) -> x.getFilename().equals(filename)).findFirst();
		if(!patternSet.isPresent())
			throw new RuntimeException("patternSet not found");
		if(patternSet.get().getFilenameOccurrences() == null || patternSet.get().getFilenameOccurrences().equals("")) {
			throw new RuntimeException("patternSet occurrences not found");
		}
		double thresholdDouble = Double.parseDouble(threshold);
		if(thresholdDouble > 1.0 || thresholdDouble <= 0.0) {
			throw new IllegalArgumentException("Threshold must be in (0.0,1.0]");
		}
		//2. transform
		File patternFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilename());
		File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilenameOccurrences());
		Table patternsTable = CSVUtils.loadCSV(patternFile);
		Table patternOccurrencesTable = CSVUtils.loadCSV(patternOccFile);
		//like:
		//Window, PatternId (1-indexed)
		//0, 1
		//0, 5
		//1, 1
		//1, 6
		SetMap<Integer,Integer> pattern2window = new SetMap<>();
		for(int i=1; i<patternOccurrencesTable.getRows().size();i++) {
			Integer window = Integer.valueOf(patternOccurrencesTable.getRows().get(i).get(0));
			Integer patternId = Integer.valueOf(patternOccurrencesTable.getRows().get(i).get(1));
			pattern2window.put(patternId, window);
		}
		Set<Integer> redundantPatterns = new HashSet<Integer>();
		for(int i=1; i<patternsTable.getRows().size(); i++) {
			Set<Integer> windows1 =  pattern2window.get(i);
			for(int j=i+1; j<patternsTable.getRows().size(); j++) {
				Set<Integer> windows2 =  pattern2window.get(j);
				Set<Integer> intersection = new HashSet<Integer>(windows1); 
				intersection.retainAll(windows2);
				double jaccardIndex = intersection.size() / (double)(windows1.size() + windows2.size() - intersection.size());
				if(jaccardIndex > thresholdDouble) {
					redundantPatterns.add(j);
				}
				if(i==1 && j==2) {
					System.out.format("Info: Removing redundant patterns using Jaccard.\n" + 
								"Pattern 1 windows: %s\n" + 
								"Pattern 2 windows: %s\n" + 
								"JaccardIndex: %d / (%d + %d -%d)\n" + 
								"Threshold: %.3f>%.3f\n", 
								windows1, windows2, intersection.size(), 
								windows1.size(), windows2.size(), intersection.size(),
								jaccardIndex, thresholdDouble
							);
				}
			}
		}
		Table newTable = new Table();
		newTable.addRow(patternsTable.getRows().get(0));
		for(int i=1; i<patternsTable.getRows().size();i++) {
			if(!redundantPatterns.contains(i))
				newTable.addRow(patternsTable.getRows().get(i));
		}
		//3. save file + update stack operations + update occurrences
		CSVUtils.saveTable(newTable, patternFile);
		patternSet.get().setNoPatterns((long)newTable.getRows().size() -1);
		current_item.getStackOperations().add(String.format("Remove redundant(set=%s, jaccard_threshold=%s)",
				patternSet.get().getLabel(), threshold));
		repository.save();
		File occFile = storePatternSetOccurrences(patternFile.getName(),request);
		savePatternsWithMetadata(patternFile, occFile, current_item.getFile());
	}

	@RequestMapping(value="/rest/mining/filter-time-constraint", method=RequestMethod.POST)
	public @ResponseBody void filterPatternsOccOnTimeContraints(
			@RequestParam("filename") String filename,
			@RequestParam("maxgap") String maxgap,
			@RequestParam("maxspan") String maxspan,
			HttpServletRequest request) throws Exception
	{
		//1. get input data
		MySession mySession = (MySession) request.getSession().getAttribute("mySession");
		FileItem current_item = repository.findItemById(mySession.getCurrentItem());
		Optional<PatternSet> patternSet = current_item.getPatterns().stream().filter((x) -> x.getFilename().equals(filename)).findFirst();
		if(!patternSet.isPresent())
			throw new RuntimeException("patternSet not found");
		if(patternSet.get().getFilenameOccurrences() == null || patternSet.get().getFilenameOccurrences().equals("")) {
			throw new RuntimeException("patternSet occurrences not found");	}
		int maxgapInt = -1;
		int maxspanInt = -1;
		if(!maxgap.trim().isEmpty()) {
			maxgapInt = Integer.parseInt(maxgap);
		}
		if(!maxspan.trim().isEmpty()) {
			maxspanInt= Integer.parseInt(maxspan);
		}
		//2. transform
		File patternFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilename());
		File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilenameOccurrences());
		List<Triple<Integer,Integer,List<Integer>>> occurrencesWithSpan = 
				makePatternOccurrencesSpan.makeExactPatternOccurrencesWithSpan(current_item.getFile(), patternSet.get());
		//filter all occurrences that have gap > maxgap
		List<Triple<Integer,Integer,List<Integer>>> occurrencesWithSpanFiltered = new ArrayList<>(); 
		for(Triple<Integer,Integer,List<Integer>> occurrenceWithSpan: occurrencesWithSpan) {
			//compute span
			List<Integer> timestamps = occurrenceWithSpan.getThirth();
			int currentSpan = Collections.max(timestamps) - Collections.min(timestamps) + 1;
			//compute max gap
			//TODO NOT ENTIRELY CORRECT FOR ITEMSETS, e.g. can be another window with large span, but with smaller mingap
			int currentMaxGap = -1;
			if(patternSet.get().getType().equals("itemsets")) {
				Collections.sort(timestamps); //sort on time
			}
			for(int i=0; i<timestamps.size()-1;i++) {
				int currentGap = timestamps.get(i+1) - timestamps.get(i);
				currentMaxGap = Integer.max(currentMaxGap, currentGap); 
			}
			// filter on maxgap
			boolean removeOnMaxgap = (maxgapInt != -1 && currentMaxGap > maxgapInt);
			boolean removeOnMaxspan = (maxspanInt != -1 && currentSpan > maxspanInt);
			if(!removeOnMaxgap && !removeOnMaxspan) {
				occurrencesWithSpanFiltered.add(occurrenceWithSpan);
			}
			if(occurrenceWithSpan.getFirst().equals(0) && occurrenceWithSpan.getSecond() < 3) {
				System.out.format("Info: Filtering on occurences with constraints: Window 0, Pattern %d\n" + 
								   "Timestamps: %s\nSpan:%d Maxpan:%d Gap:%d Maxgap: %d\n",
								   occurrenceWithSpan.getSecond(), timestamps, currentSpan, maxspanInt, currentMaxGap, maxgapInt);
			}
		}
		System.out.format("Filtering on occurrences: Before: %d, After: %d\n", 
				occurrencesWithSpan.size(), occurrencesWithSpanFiltered.size());
		//re-compute support
		CountMap<Integer> supports = new CountMap<>();
		for(Triple<Integer,Integer,List<Integer>> occurrenceWithSpan: occurrencesWithSpanFiltered) {
			int patternId = occurrenceWithSpan.getSecond();
			supports.add(patternId);
		}
		//3. save file + update stack operations + update occurrences
		//3.1. Update support in patterns tables
		Table patternsTable = CSVUtils.loadCSV(patternFile);
		List<Pair<String,Integer>> newPatterns = new ArrayList<>();
		for(int i=1; i<patternsTable.getRows().size(); i++) {
			String pattern = patternsTable.getRows().get(i).get(0);
			int newSupport = supports.get(i);
			//remove patterns if support == 0, e.g. all occurrences have large span, than maxspan
			if(newSupport != 0) {
				newPatterns.add(new Pair<>(pattern,newSupport));
			}
		}
		Collections.sort(newPatterns, new ArffToSPMF.PatternComparator()); //sort again
		Table newTable = new Table();
		newTable.addRow(patternsTable.getRows().get(0));
		for(Pair<String,Integer> pattern: newPatterns) {
			newTable.addRow(Arrays.asList(pattern.getFirst(), pattern.getSecond().toString()));
		}
		CSVUtils.saveTable(newTable, patternFile);
		//3.2 Save occurrences
		Table table = new Table();
		table.addRow(Arrays.asList("Window", "PatternId"));
		for(Triple<Integer,Integer,List<Integer>> occurrence: occurrencesWithSpanFiltered) {
			int window = occurrence.getFirst();
			int patternId = occurrence.getSecond();
			table.addRow(Arrays.asList(String.valueOf(window),String.valueOf(patternId)));
		}
		CSVUtils.saveTable(table, patternOccFile);
		//+ save pattern metadata
		savePatternsWithMetadata(patternFile, patternOccFile, current_item.getFile());
		//3.3. Add stack op
		patternSet.get().setNoPatterns((long)newTable.getRows().size() -1);
		current_item.getStackOperations().add(String.format("Filter occurrences on time constraints(set=%s,gap=%s,span=%s)",
				patternSet.get().getLabel(), maxgap, maxspan));
		repository.save();
	}

	@RequestMapping(value="/rest/mining/anomaly-detection-fpof", method=RequestMethod.POST)
	public @ResponseBody String anomalyDetectionFPOF(
			@RequestParam("patternFilenames") String patternFilenames,
			HttpServletRequest request) throws Exception
	{
		//1. get input
		MySession mySession = (MySession) request.getSession().getAttribute("mySession");
		FileItem current_item = repository.findItemById(mySession.getCurrentItem());
		String[] patternFilenamesArr = patternFilenames.split(",\\s*");
		//2. compute FPOF for each window
		Timer timer = new Timer("FPOF");
		int totalPatterns = 0;
		CountMap<Integer> countsPerWindow = new CountMap<>(true);
		List<String> labelsPatternSets = new ArrayList<>();
		for(String patternFilename: patternFilenamesArr) {
			Optional<PatternSet> patternSet = current_item.getPatterns().stream().filter((x) -> x.getFilename().equals(patternFilename)).findFirst();
			if(!patternSet.isPresent())
				throw new RuntimeException("patternSet not found " + patternFilename);
			if(patternSet.get().getFilenameOccurrences() == null || patternSet.get().getFilenameOccurrences().equals("")) {
				throw new RuntimeException("patternSet occurrences not found " + patternSet.get().getFilenameOccurrences());
			}
			labelsPatternSets.add(patternSet.get().getLabel());
			//compute FPOF for each window
			long noPatterns = patternSet.get().getNoPatterns();
			totalPatterns += noPatterns;
			//Note: Original FPOP work on itemsets, not on mix of different pattern types
			File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.get().getFilenameOccurrences());
			Table occ = CSVUtils.loadCSV(patternOccFile);
			for(int i=1; i<occ.getRows().size(); i++) {
				int windowId = Integer.valueOf(occ.getRows().get(i).get(0));
				//int patternId = Integer.valueOf(occ.getRows().get(i).get(1));
				countsPerWindow.add(windowId);
				if(i == 1) {
					System.out.format("info: FPOF window 0, after set %s, is %d/%d\n", patternSet.get().getLabel(), 
							countsPerWindow.get(windowId), totalPatterns);
				}
			}
		}
		List<Pair<Integer,Float>> fpofFactors = new ArrayList<>();
		int lastWindow = Collections.max(countsPerWindow.getMap().keySet());//technically not correct...
		for(int windowId =0; windowId <lastWindow ; windowId++) {
			if(windowId == 0) {
				System.out.format("info: FPOF window 0 is %d/%d\n", countsPerWindow.get(windowId), totalPatterns);
			}
			float fpof = countsPerWindow.get(windowId) / (float)totalPatterns; //countsPerWindow can be empty (return 0), e.g. if no patterns match!
			fpofFactors.add(new Pair<>(windowId,fpof));
		}
		//3. save results
		//3.1. save scores
		String label = "FPOF(" + CollectionUtils.join(labelsPatternSets, ";") + ")"; //e.g. FPOF(maximal itemsets pc1,pc2; sequential patterns pc1)
		String suffix = "_scores_" + label.replaceAll("[\\(\\);\\,\\s+]", "_") + ".csv";
		File scoreFile = new File(Settings.FILE_FOLDER + current_item.getId() + suffix);
		Table tableScores = new Table();
		tableScores.addRow(Arrays.asList("Window","Score"));
		for(Pair<Integer,Float> score: fpofFactors) {
			tableScores.addRow(Arrays.asList(score.getFirst().toString(), String.format("%.6f", score.getSecond())));
		}
		CSVUtils.saveTable(tableScores, scoreFile);
		long elapsed = timer.elapsed();
		//3.2 get evaluation metrics
		Map<String,Double> results = computeAnomalyEvaluationMetrics(scoreFile, current_item.getFile(), "FPOF");
		label = label.substring(0,label.length()-1) + String.format("; AUC=%.3f, AP=%.3f)", results.get("AUC"), results.get("AP"));
		//3.3  save project
		AnomalyScores score = new AnomalyScores();
		score.setAlgorithm("FPOF");
		score.setLabel(label);
		score.setFilenamesPatternSets(Arrays.asList(patternFilenamesArr));
		score.setFilename(scoreFile.getName());
		score.setEvaluationResults(results);
		current_item.getScores().add(score);
		current_item.getStackOperations().add("Anomaly detection " + label);
		repository.save();
		return String.format("Finished FPOF. Took %s. AUC=%.3f, AP=%.3f", Utils.milisToStringReadable(elapsed), results.get("AUC"), results.get("AP"));
	}
	
	@RequestMapping(value="/rest/mining/anomaly-detection-pbad", method=RequestMethod.POST)
	public @ResponseBody String anomalyDetectionPBAD(
			@RequestParam("patternFilenames") String patternFilenames,
			HttpServletRequest request) throws Exception
	{
		//1. get input
		MySession mySession = (MySession) request.getSession().getAttribute("mySession");
		FileItem current_item = repository.findItemById(mySession.getCurrentItem());
		String[] patternFilenamesArr = patternFilenames.split(",\\s*");
		List<String> labelsPatternSets = new ArrayList<>();
		for(String patternFilename: patternFilenamesArr) {
			Optional<PatternSet> patternSet = current_item.getPatterns().stream().filter((x) -> x.getFilename().equals(patternFilename)).findFirst();
			if(!patternSet.isPresent())
				throw new RuntimeException("patternSet not found " + patternFilename);
			labelsPatternSets.add(patternSet.get().getLabel());
		}
		//2. compute PBAD for each window
		String label = "PBAD(" + CollectionUtils.join(labelsPatternSets, ";") + ")";
		String suffix = "_scores_" + label.replaceAll("[\\(\\);\\,\\s+]", "_") + ".csv";
		File scoreFile = new File(Settings.FILE_FOLDER + current_item.getId() + suffix);
		Timer timer = new Timer("PBAD");
		runPBADEmbeddingOnly.computePBADScore(current_item, scoreFile);
		long elapsed = timer.end();
		//2.2 get evaluation metrics
		Map<String,Double> results = computeAnomalyEvaluationMetrics(scoreFile, current_item.getFile(),"PBAD");
		label = label.substring(0,label.length()-1) + String.format("; AUC=%.3f, AP=%.3f)", results.get("AUC"), results.get("AP"));
		//3. save results
		AnomalyScores score = new AnomalyScores();
		score.setAlgorithm("PBAD");
		score.setLabel(label);
		score.setFilenamesPatternSets(Arrays.asList(patternFilenamesArr));
		score.setFilename(scoreFile.getName());
		score.setEvaluationResults(results);
		current_item.getScores().add(score);
		current_item.getStackOperations().add("Anomaly detection " + label);
		repository.save();
		return String.format("Finished PBAD. Took %s. AUC=%.3f, AP=%.3f", Utils.milisToStringReadable(elapsed), results.get("AUC"), results.get("AP"));
	}

	private Map<String,Double> computeAnomalyEvaluationMetrics(File scoreFile, File itemFile, String method) throws IOException, InterruptedException {
		Map<Integer, Integer> window2LabelMap = getWindowLabels(itemFile);
		//Window,Score
		//0,0.246154
		//1,0.093706
		//2,0.030769
		Table scores = CSVUtils.loadCSV(scoreFile);
		scores.getRows().get(0).add("Label");
		for(List<String> row: scores.getRowsStartingFrom1()) {
			Integer window = Integer.valueOf(row.get(0));
			Double score = Double.valueOf(row.get(1));
			Integer label = window2LabelMap.get(window);
			row.add(String.valueOf(label));
		}
		File output = new File(scoreFile.getAbsolutePath() + "_with_label.csv");
		CSVUtils.saveTable(scores, output);
		//run python, since sklearn.metrics has everything
		File log = new File("./temp/eval_log.txt");
		//check evaluation script and python are installed
		File evalPython = new File("./src/main/python/compute_anomaly_evaluation.py").getAbsoluteFile();
		if(!new File(Settings.PYTHON3).exists()) {
			String msg = String.format("Evaluation depends on Python3. Please download and install Python3.\n" + 
					"Location of Python3 is configured in be.uantwerpen.mime_webapp.Settings.\n" + 
					"Expected location of Python3 is: %s", new File(Settings.PYTHON3).getAbsolutePath());
			System.err.println(msg);
		}
		if(!evalPython.exists()) {
			String msg = String.format("Program to compute evaluation not found.\n" + 
						"Expected location PBAD is: %s", evalPython);
			System.err.println(msg);
			throw new RuntimeException(msg);
		}
		//run 
		String[] command = new String[]{Settings.PYTHON3, evalPython.getAbsolutePath(), output.getAbsolutePath(), method};
		int status = CommandLineUtils.runCommandInUserDir(command, log, 10);
		if(status != 0) {
			String logStr = IOUtils.readFileFlat(log);
			System.err.println("Error running evaluation. Log:");
			System.err.println(logStr);
			throw new RuntimeException("Error running evaluation: " + logStr);
		}
		else {
			List<String> logStr = IOUtils.readFile(log);
			System.out.println(CollectionUtils.join(logStr));
			//last two lines contain AUX and AP, parse
			//>Running /usr/local/bin/python3 compute_anomaly_evaluation.py blabla_scores_FPOF_maximal_itemsets_pc1_.csv_with_label.csv
			//Argument List:['compute_anomaly_evaluation.py', 'blabla_scores_FPOF_maximal_itemsets_pc1_.csv_with_label.csv']
			//AUC: 0.442
			//AP: 0.259
			//<Finished took  1.04 seconds

			Map<String,Double> evalMap = new HashMap<String,Double>();
			try {
				Double auc = Double.valueOf(logStr.get(logStr.size()-3).split(":")[1]);
				Double ap = Double.valueOf(logStr.get(logStr.size()-2).split(":")[1]);
				//last line 
				evalMap.put("AUC", auc);
				evalMap.put("AP",ap);
				return evalMap;
			}catch(Exception e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	private void savePatterns(HttpSession httpSession, File arffFile, File patternFile, String type, String columns, String algorithm, String support){ 
		try {
			MySession mySession = (MySession) httpSession.getAttribute("mySession");
			Project projectObj = repository.findByName(mySession.getCurrentProject());
			FileItem current_item = repository.findItemById(mySession.getCurrentItem());
			//make pattern set object
			PatternSet patternSet = new PatternSet();
			patternSet.setColumns(columns);
			patternSet.setAlgorithm(algorithm);
			patternSet.setSupport(support);
			patternSet.setNoPatterns(IOUtils.countLines(patternFile)-1);
			patternSet.setFilename(patternFile.getName());
			patternSet.setType(type);
			//save item 
			FileItem item = current_item.clone();
			item.setVersion(item.getVersion()+1);
			item.add(patternSet);
			String transform = "Mine " + (type.equals("itemsets")?"itemsets":"sequential patterns") + "(" + columns + ", " + algorithm  + ", " + support + "%)";
			item.getStackOperations().add(transform);
			projectObj.add(item);
			repository.saveProject(projectObj);
			//set current session
			httpSession.setAttribute("mySession", new MySession(projectObj.getName(), item.getId()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	private FileItem getCurrentItem(HttpSession httpSession){
		MySession session = (MySession) httpSession.getAttribute("mySession");
		String id = session.getCurrentItem();
		return repository.findItemById(id);
	}
}
