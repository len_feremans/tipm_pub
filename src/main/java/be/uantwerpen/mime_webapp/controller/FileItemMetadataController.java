package be.uantwerpen.mime_webapp.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.ldataminining.preprocessing.ArffStatistics;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.FindNullPattern;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.MySession;

@RestController
public class FileItemMetadataController {

	@Autowired
	ProjectRepository repository;

	/**
	 * returns name/type for all attributes
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/rest/metadata/attributes", method=RequestMethod.GET)
	public @ResponseBody List<Map<String,String>> getSchema(HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		if(!item.isArff()) {
			System.err.println("Only arff supported");
			return new ArrayList<>();
		}
		try {
			List<String> types = ArffUtils.getAttributeValues(item.getFile());
			List<String> names =  ArffUtils.getAttributeNames(item.getFile());
			List<Map<String,String>> metadata = new ArrayList<Map<String,String>>();
			for(int i=0; i<names.size(); i++){
				metadata.add(CollectionUtils.asMap(new String[][]{{names.get(i), types.get(i)}}));
			}
			return metadata;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	used?
	@RequestMapping(value="/rest/metadata/global-statistics", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> getSchemaStatistics(
			HttpServletRequest request)
	{
		MySession session = (MySession) request.getSession().getAttribute("mySession");
		File currentFile = new File(Settings.FILE_FOLDER + session.getCurrentData());
		if(!currentFile.getName().endsWith("arff"))
			throw new RuntimeException("Only arff supported");
		try {
			Map map = ArffStatistics.getStatistics(currentFile);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	*/
	
	@RequestMapping(value="/rest/metadata/attribute-and-statistics", method=RequestMethod.GET)
	public @ResponseBody List<Map<String,String>> getAttributeStatistics(HttpServletRequest request)
	{
		System.out.println("FileItemMetadata>>Load-attribute-and-statistics");
		FileItem currentItem = getCurrentItem(request.getSession());
		if(currentItem == null) {
			return null;
		}
		if(!currentItem.isArff())
			return null;
		try {
			List<Map<String,String>> map = ArffStatistics.getAllAttributeStatisticsFast(currentItem.getFile());
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error computing statistics: " + e.getMessage());
		}
	}
	
	
	@RequestMapping(value="/rest/metadata/nullpatterns", method=RequestMethod.GET)
	public @ResponseBody List<List<String>> getNullPatterns(HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		if(!item.isArff()) {
			System.err.println("Only arff supported");
			return null;
		}
		try {
			List<List<String>> map = FindNullPattern.findNullPattern(item.getFile(),10);
			return map;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@RequestMapping(value="/rest/metadata/fileitem", method=RequestMethod.GET)
	public @ResponseBody FileItem getFileItem(HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		else
			return item;
	}
	
	
	
	private FileItem getCurrentItem(HttpSession httpSession){
		MySession session = (MySession) httpSession.getAttribute("mySession");
		if(session == null)
			return null;
		String id = session.getCurrentItem();
		return repository.findItemById(id);
	}

}
