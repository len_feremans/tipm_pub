package be.uantwerpen.mime_webapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.MySession;
import be.uantwerpen.mime_webapp.model.Project;

@RestController
public class DataManagementController {

	@RequestMapping("/")
	public String index() {
		return  "<html><head><meta http-equiv=\"refresh\" content=\"1;url=/list\"/></head><body>Welcome!</body></html>";
		
	}

	@Autowired
	private ProjectRepository repository;

	@RequestMapping(value="/rest/projects", method = RequestMethod.GET)
	public @ResponseBody List<Project> getAllProjects()
	{
		List<Project> projects = repository.findAll();
		return projects;
	}
	
	@RequestMapping(value="/rest/project/{name}", method = RequestMethod.GET)
	public @ResponseBody Project getProject( @PathVariable("name") String name)
	{
		return repository.findByName(name);
	}

	@RequestMapping(value="/rest/item/set-current", method=RequestMethod.GET)
	public @ResponseBody void setCurrent(
			@RequestParam("project") String project,
			@RequestParam("item") String itemId,
			HttpServletRequest request)
	{
		if(itemId.equals("") || project.equals(""))
			return;
		System.out.println("rest>>set-current(" + project + "," + itemId + ")");
		//Access database to get filename
		FileItem item = repository.findItemById(itemId);
		if(!item.isArff() && !item.isCSV())
			throw new RuntimeException("Only ARRF or CSV input expected");
		request.getSession().setAttribute("mySession",new MySession(project,itemId));
	}
		
	@RequestMapping(value="/rest/item/remove", method = RequestMethod.POST)
	public @ResponseBody void removeItem(
				@RequestParam("item") String itemId,
				HttpServletRequest request)
	{
		FileItem removedItem = repository.removeItem(itemId);
		for(Project project: repository.findAll()){
			for(FileItem item: project.getFileItems()){
				if(item.getLogicalName().equals(removedItem.getLogicalName())
				   && item.getVersion() == removedItem.getVersion() - 1) {
					//Found previous version, save as current
					request.getSession().setAttribute("mySession",new MySession(project.getName(),item.getId()));
				}
			}
		}
	}

	@RequestMapping(value="/rest/project/create", method=RequestMethod.POST)
	public @ResponseBody Project addProject( @RequestParam("name") String name )
	{
		Project project = getProject(name);
		if(project !=null)
		{
			throw new RuntimeException("Project with same name already exists");
		}
		return repository.saveProject(new Project( name ) );
	}
	
}
