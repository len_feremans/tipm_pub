package be.uantwerpen.mime_webapp.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.datamining.pattern_mining.MakePatternOccurrences;
import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.AnomalyScores;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.MySession;
import be.uantwerpen.mime_webapp.model.PatternSet;

@RestController
public class TableLoadController {

	@Autowired
	ProjectRepository repository;

	/**
	 *  CSV or ARFF
	 */
	@RequestMapping(value="/rest/load-data", method=RequestMethod.GET)
	public @ResponseBody Table loadData(@RequestParam("pagination") String pagination, HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		//Access database to get filename
		if(!item.isArff() && !item.isCSV())
			throw new RuntimeException("Only ARRF or CSV input expected");
		System.out.println("rest>>loadData(" + item.getId() + "," + item + ")");
		//Load ARRF or CSV file
		if(pagination.equals("")){
			pagination = "0-1000"; //default only first 1000 rows read
		}
		int from = Math.max(0, Integer.valueOf(pagination.split("-")[0]));
		int to = Integer.valueOf(pagination.split("-")[1]);
		try {
			Table table = null;
			if(item.isArff()){
				table = new ArffDenseUtils().loadArff(item.getFile(), from, to);
				/*
				In comment: formatting in UI / javascript
				for(List<String> row: table.getRows()) {
					for(int i=0; i<row.size(); i++) {
						String value = row.get(i);
						if(Utils.isFloat(value)) {
							value = String.format("%.3", Utils.asDouble(value));
							row.set(i, value);
						}
					}
				}
				 */
			}
			else{
				table = CSVUtils.loadCSV(item.getFile(), from, to);
			}
			return table;
		}catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value="/rest/load-data-windows", method=RequestMethod.GET)
	public @ResponseBody Table loadDataWindows(@RequestParam("pagination") String pagination, HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		//Access database to get filename
		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");
		System.out.println("rest>>loadDataWindows(" + item.getId() + "," + item + ")");
		//Load ARRF or CSV file
		if(pagination.equals("")){
			pagination = "0-1000"; //default only first 1000 rows read
		}
		int from = Math.max(0, Integer.valueOf(pagination.split("-")[0]));
		int to = Integer.valueOf(pagination.split("-")[1]);
		File f = item.getFile();
		try {
			Table tab = MakePatternOccurrences.groupByWindow(f,from,to);
			return tab;
		}catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value="/rest/load-patterns", method=RequestMethod.GET)
	public @ResponseBody Table loadPatterns(
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");
		if(item.getPatterns().size() == 0 || item.getPatterns().stream().filter((p) -> p.getFilename().equals(filename)).findFirst() == null) {
			throw new RuntimeException("Unable to find pattern set with filename == " + filename);
		}
		try {
			System.out.println("rest>>loadPatterns(" + item.getLogicalName() + "," + filename + ")");	
			Table table = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + filename));
			return table;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@RequestMapping(value="/rest/load-patterns-metadata", method=RequestMethod.GET)
	public @ResponseBody Table loadPatternsMetadata(
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");
		if(item.getPatterns().size() == 0 || item.getPatterns().stream().filter((p) -> p.getFilename().equals(filename)).findFirst() == null) {
			throw new RuntimeException("Unable to find pattern set with filename == " + filename);
		}
		try {
			System.out.println("rest>>loadPatternsMetadata(" + item.getLogicalName() + "," + filename + ")");	
			Table table = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + filename + "_metadata.csv"));
			return table;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@RequestMapping(value="/rest/load-pattern-occ", method=RequestMethod.GET)
	public @ResponseBody Table loadPatternOccurrences(
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");
		Optional<PatternSet> patternSet = item.getPatterns().stream().filter((p) -> p.getFilename().equals(filename)).findFirst();
		if(!patternSet.isPresent()) {
			throw new RuntimeException("Unable to find pattern set with filename == " + filename);
		}
		if(patternSet.get().getFilenameOccurrences() == null || patternSet.get().getFilenameOccurrences().isEmpty()) {
			throw new RuntimeException("No occurrences are available for " + filename);
		}
		try {
			System.out.println("rest>>loadPatternsOccurrences(" + item.getLogicalName() + "," + filename + ")");	
			Table table = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + patternSet.get().getFilenameOccurrences()));
			return table;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@RequestMapping(value="/rest/load-anomaly-score", method=RequestMethod.GET)
	public @ResponseBody Table loadAnomalyScore(
			@RequestParam("filename") String filename,
			HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request.getSession());
		if(item == null)
			return null;
		if(!item.isArff())
			throw new RuntimeException("ARRF input expected");
		Optional<AnomalyScores> anomalyScore = item.getScores().stream().filter((p) -> p.getFilename().equals(filename)).findFirst();
		if(!anomalyScore.isPresent()) {
			throw new RuntimeException("Unable to find anomaly score set with filename == " + filename);
		}
		try {
			System.out.println("rest>>loadAnomalyScore(" + item.getLogicalName() + "," + filename + ")");	
			Table table = CSVUtils.loadCSV(new File(Settings.FILE_FOLDER + anomalyScore.get().getFilename()));
			return table;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private FileItem getCurrentItem(HttpSession httpSession){
		MySession session = (MySession) httpSession.getAttribute("mySession");
		if(session == null)
			return null;
		String id = session.getCurrentItem();
		return repository.findItemById(id);
	}

}
